# Rapid Reader Plugin

This is the Rapid Viewer Plugin. It contains the UIs for Rapid Reader (/rapid-reader), and exposes APIs to manage work lists and work items in them.
The main consumer of the APIs is the Rapid viewer, but the APIs can be used elsewhere.

# Building

To build the Rapid Reader plugin:

1. If you haven't already, clone this repository and cd to the newly cloned folder.
1. Clone submodule: git submodule update --init
1. Build the plugin: `./gradlew fatJar` (on Windows, you can use the batch file: `gradlew.bat fatJar`). This should build the plugin in the file **build/libs/rapid-reader-plugin-\*-fat.jar** (the version may differ based on updates to the code).
1. Copy the plugin jar to your plugins folder: `cp build/libs/rapid-reader-plugin-*.jar /data/xnat/home/plugins`

# Configuration

## plugins that this plugin depends on

1. OHIF-Viewer Plugin for XNANT: https://bitbucket.org/icrimaginginformatics/ohif-viewer-xnat-plugin/downloads/ohif-viewer-3.0.0-XNAT-1.8.0.jar
1. A datatype plugin for the form part of the rapid reader should be installed. If you don't have yet, you can test with a sample plugin : https://bitbucket.org/xnatx/rapid-viewer-plugin/downloads/rapid-reader-sample-datatype-plugin-1.0.0-SNAPSHOT.jar

## Set up a work list and items

1. Please make sure that this plugin and the dependent plugins are installed into the XNAT you are testing.
1. run the following command to create a work list
   curl -u [username]:[password] -X POST [xnat_url]/xapi/workLists/ -H 'Content-Type: application/json' -d '{"readerUsername":"[xnat_usernamme]","dueDate": 1663719840107,"name":"test","description":"test", "reportId":"RPT50524", "status":"Open"}'
1. run the following comands repeatedly to create work items to above the list.
   curl -u "admin:admin" -X POST [xnat_url]/xapi/workLists/[work-list-id]/items -H 'Content-Type: application/json' -d '{"experimentId":"[experientid - e.g., XNAT_E00001]"}'

## run rapid-reader.

1. Login into the XNAT using the username you set as a readerUsername.
2. Navigate to the [xnat_url]/rapid-reader
