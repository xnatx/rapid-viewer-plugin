/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem.WorkItemStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkListTime;
import org.nrg.xnatx.plugins.rapidReader.repositories.WorkItemRepository;
import org.nrg.xnatx.plugins.rapidReader.repositories.WorkListTimeRepository;
import org.nrg.xnatx.plugins.rapidReader.services.WorkItemService;
import org.nrg.xnatx.plugins.rapidReader.services.WorkListService;
import org.nrg.xnatx.plugins.rapidReader.services.WorkListTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Manages {@link WorkList} data objects in Hibernate.
 */
@Service
@Slf4j
public class HibernateWorkTimeService extends AbstractHibernateEntityService<WorkListTime, WorkListTimeRepository>
        implements WorkListTimeService {
    @Transactional
    @Override
    public List<WorkListTime> getWorkListTimes(final Long workListId) {
        return _dao.getWorkListTimes(workListId);
    }

    @Transactional
    @Override
    public WorkListTime createWorkListTime(Long workListId) throws NotFoundException {
        WorkListTime workListTime = new WorkListTime();
        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
        }

        workListTime.setWorkList(workList);
        workListTime.setBeginDate(new Date());
        workListTime.setEndDate(new Date());
        _dao.create(workListTime);
        return null;
    }

    @Transactional
    @Override
    public WorkListTime updateWorkListTime(Long workListTimeId, Boolean abnormallyStopped, String stopReason) throws NotFoundException {
        WorkListTime workListTime = _dao.findById(workListTimeId);
        if (workListTime == null) {
            throw new NotFoundException("No workListTime with the ID \"" + workListTimeId + "\" was found.");
        }

        workListTime.setEndDate(new Date());
        workListTime.setAbnormallyStopped(abnormallyStopped);
        workListTime.setStopReason(stopReason);
        _dao.update(workListTime);
        return workListTime;
    }

    @Autowired
    private WorkListTimeRepository _dao;

    @Autowired
    private WorkListService _workListService;
}
