/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "xdatUserId" }))
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class RapidReaderUser extends AbstractHibernateEntity {
	private static final long serialVersionUID = 1L;

	@NotNull
	Integer _xdatUserId;
}
