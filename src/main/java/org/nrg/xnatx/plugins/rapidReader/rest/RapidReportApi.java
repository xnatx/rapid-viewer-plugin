/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.rest;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnatx.plugins.rapidReader.dto.*;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.CacheControl;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

@Api
@XapiRestController
@RequestMapping(value = "/reports")
@Slf4j
public class RapidReportApi extends AbstractXapiRestController {
    private final UserManagementServiceI _userManagementService;
    private final ReportService _reportService;

    @Autowired
    protected RapidReportApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder, final ReportService reportService) {
        super(userManagementService, roleHolder);
        _userManagementService = userManagementService;
        _reportService = reportService;
    }

    @ApiOperation(value = "Produce a zip file that contains the work-list report file and its checksum file")
    @ApiResponses({@ApiResponse(code = 200, message = "The zip file successfully produced."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "workLists", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE}, method = RequestMethod.GET, restrictTo = Admin)
    public ResponseEntity<Resource> produceWorkListReport(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "workListIds", required = false) @RequestParam(required = false) final List<Long> workListIds,
            @ApiParam(value = "projectId", required = false) @RequestParam(required = false) final String projectId,
            @ApiParam(value = "includesHeader", required = false) @RequestParam(required = false, defaultValue = "false") final Boolean includesHeader
    ) throws IOException, NoSuchAlgorithmException, NotFoundException {
        log.info("{}?{} [GET] called", request.getRequestURI(), request.getQueryString());

        String fileNameWithoutExtension = _reportService.generateFileNameWithoutExtension(WorkListReportDto.class);
        String zipFileName = fileNameWithoutExtension + ".zip";

        byte[] zipBytes =
                workListIds != null && workListIds.size() > 0 ?
                        _reportService.createZippedWorkListReport(workListIds, fileNameWithoutExtension, includesHeader, false) :
                        _reportService.createZippedWorkListReport(projectId, fileNameWithoutExtension, includesHeader);
        response.setHeader("Content-Disposition", "attachment; filename=" + zipFileName);

        ByteArrayResource resource = new ByteArrayResource(zipBytes);
        return ResponseEntity.ok()
                .cacheControl(CacheControl.noCache())
                .contentLength(zipBytes.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    @ApiOperation(value = "Produce a zip file that contains the work-item report file and its checksum file")
    @ApiResponses({@ApiResponse(code = 200, message = "The zip file successfully produced."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "workItems/", produces = {MediaType.APPLICATION_OCTET_STREAM_VALUE}, method = RequestMethod.GET, restrictTo = Admin)
    public ResponseEntity<Resource> produceWorkItemReport(
            HttpServletRequest request,
            HttpServletResponse response,
            @ApiParam(value = "workListIds", required = false) @RequestParam(required = false) final List<Long> workListIds,
            @ApiParam(value = "projectId", required = false) @RequestParam(required = false) final String projectId,
            @ApiParam(value = "includesHeader", required = false) @RequestParam(required = false, defaultValue = "false") final Boolean includesHeader
    ) throws IOException, NoSuchAlgorithmException, NotFoundException {
        log.info("{}?{} [GET] called", request.getRequestURI(), request.getQueryString());

        String fileNameWithoutExtension = _reportService.generateFileNameWithoutExtension(WorkItemReportDto.class);
        String zipFileName = fileNameWithoutExtension + ".zip";

        byte[] zipBytes =
                workListIds != null && workListIds.size() > 0 ?
                        _reportService.createZippedWorkItemReport(workListIds, fileNameWithoutExtension, includesHeader, false) :
                        _reportService.createZippedWorkItemReport(projectId, fileNameWithoutExtension, includesHeader);

        response.setHeader("Content-Disposition", "attachment; filename=" + zipFileName);

        ByteArrayResource resource = new ByteArrayResource(zipBytes);
        return ResponseEntity.ok()
                .cacheControl(CacheControl.noCache())
                .contentLength(zipBytes.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}
