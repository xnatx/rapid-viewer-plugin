/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.repositories;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment.CommentStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class RapidCommentRepository extends AbstractHibernateDAO<RapidComment> {
	@SuppressWarnings("unchecked")
	@Transactional
	public List<RapidComment> getComments(Long workItemId, String orderBy, boolean isAsc, Integer offset,
			Integer limit) {
		final Criteria criteria = getSession().createCriteria(getParameterizedType());
		criteria.add(Restrictions.eq("workItem.id", workItemId));
		criteria.addOrder(isAsc ? Order.asc(orderBy) : Order.desc(orderBy));
		criteria.setFirstResult(offset);
		criteria.setMaxResults(limit);
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<RapidComment> getCommentsByStatus(Long workItemId, final CommentStatus status, String orderBy, boolean isAsc, Integer offset,
			Integer limit) {
		final Criteria criteria = getSession().createCriteria(getParameterizedType());
		criteria.add(Restrictions.eq("workItem.id", workItemId));
		criteria.add(Restrictions.eq("status", status));
		criteria.addOrder(isAsc ? Order.asc(orderBy) : Order.desc(orderBy));
		criteria.setFirstResult(offset);
		criteria.setMaxResults(limit);
		return criteria.list();
	}
}
