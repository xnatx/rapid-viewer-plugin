/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurvey;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.repositories.ReaderSurveyRepository;
import org.nrg.xnatx.plugins.rapidReader.repositories.WorkListRepository;
import org.nrg.xnatx.plugins.rapidReader.services.ReaderSurveyService;
import org.nrg.xnatx.plugins.rapidReader.services.WorkListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Manages {@link WorkList} data objects in Hibernate.
 */
@Service
@Slf4j
public class HibernateSurveyService extends AbstractHibernateEntityService<ReaderSurvey, ReaderSurveyRepository>
		implements ReaderSurveyService {
	@Transactional
	@Override
	public ReaderSurvey getSurvey(Long workListId, ReaderSurvey.SurveyType surveyType) {
		return _dao.getSurvey(workListId, surveyType);
	}

	@Autowired
	private ReaderSurveyRepository _dao;
}
