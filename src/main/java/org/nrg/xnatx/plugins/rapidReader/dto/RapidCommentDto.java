/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import java.util.HashSet;
import java.util.Set;

import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment.CommentStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class RapidCommentDto {
	private Long workItemId;

	@Builder.Default
	private Set<String> toEmails = new HashSet<String>();

	private String subject;

	private String body;

	private CommentStatus status;

	public RapidComment toComment(WorkItem item) {
		RapidComment comment = new RapidComment();
		comment.setToEmails(toEmails).setSubject(subject).setBody(body).setStatus(status);
		comment.setWorkItem(item);
		return comment;
	}
}
