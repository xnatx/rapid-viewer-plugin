/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"workListId", "experimentId"}))
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class WorkItem extends AbstractHibernateEntity {
    private static final long serialVersionUID = 1L;

    public enum WorkItemStatus {
        InProgress, Open, Partial, Complete, Cancelled, Failed
    }

    @Converter(autoApply = true)
    public static class WorkListItemStatusConverter implements AttributeConverter<WorkItemStatus, String> {

        @Override
        public String convertToDatabaseColumn(WorkItemStatus status) {
            if (status == null) {
                return null;
            }
            return status.toString();
        }

        @Override
        public WorkItemStatus convertToEntityAttribute(String code) {
            if (code == null) {
                return null;
            }

            for (WorkItemStatus status : WorkItemStatus.values()) {
                if (code.equals(status.toString())) {
                    return status;
                }
            }

            throw new IllegalArgumentException();
        }
    }

    private WorkList _workList;

    @NotNull
    private String _projectId;

    private String _projectLabel;

    @NotNull
    private String _subjectId;

    @NotNull
    private String _subjectLabel;

    @NotNull
    private String _experimentId;

    @NotNull
    private String _experimentLabel;

    @NotNull
    private WorkItemStatus _status;

    private String _reportAssessorId;

    private String _failReason;

//	@Column(columnDefinition = "text")
//	private String _fieldsJson;

    private Date _updated;

    private Integer _year;
    private Integer _month;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "workListId", nullable = false)
    public WorkList getWorkList() {
        return _workList;
    }

    @Column(columnDefinition = "TEXT")
    public String getFailReason() {
        return _failReason;
    }

    public void preCreate() {
        _status = WorkItemStatus.Open;
    }
}
