/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import java.util.List;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment.CommentStatus;

public interface RapidCommentService extends BaseHibernateService<RapidComment> {
	public List<RapidComment> getComments(final Long workItemId, String orderBy, boolean isAsc, Integer offset,
			Integer limit);

	public List<RapidComment> getCommentsByStatus(final Long workItemId, final CommentStatus status, String orderBy,
			boolean isAsc, int offset, int limit);
}
