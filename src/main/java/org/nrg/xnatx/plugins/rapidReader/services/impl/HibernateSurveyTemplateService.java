/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.repositories.ReaderSurveyRepository;
import org.nrg.xnatx.plugins.rapidReader.repositories.ReaderSurveyTemplateRepository;
import org.nrg.xnatx.plugins.rapidReader.repositories.WorkListRepository;
import org.nrg.xnatx.plugins.rapidReader.services.ReaderSurveyTemplateQuestionService;
import org.nrg.xnatx.plugins.rapidReader.services.ReaderSurveyTemplateService;
import org.nrg.xnatx.plugins.rapidReader.services.WorkListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Manages {@link WorkList} data objects in Hibernate.
 */
@Service
@Slf4j
public class HibernateSurveyTemplateService extends AbstractHibernateEntityService<ReaderSurveyTemplate, ReaderSurveyTemplateRepository>
        implements ReaderSurveyTemplateService {

    @Autowired
    public HibernateSurveyTemplateService(ReaderSurveyTemplateQuestionService questionService) {
        _questionService = questionService;
    }

    @Transactional
    @Override
    public ReaderSurveyTemplate createTemplate(ReaderSurveyTemplate template) {
        ReaderSurveyTemplate created = this.create(template);
        List<ReaderSurveyTemplateQuestion> questionsCreated = new ArrayList<>();

        List<ReaderSurveyTemplateQuestion> questions = template.getQuestions();
        for (ReaderSurveyTemplateQuestion question : questions) {
            questionsCreated.add(question.setReaderSurveyTemplate(created));
            _questionService.create(question);
        }
        created.setQuestions(questionsCreated);
        return this.create(template);
    }

    private ReaderSurveyTemplateQuestionService _questionService;
}
