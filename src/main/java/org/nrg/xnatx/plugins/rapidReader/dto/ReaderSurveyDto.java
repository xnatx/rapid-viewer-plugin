package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurvey;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class ReaderSurveyDto {
    private Long templateId;

    private ReaderSurvey.SurveyType type;

    private ReaderSurvey.SurveyStatus status;

    private String answer;

    public ReaderSurvey toReaderSurvey() {
        ReaderSurvey survey = new ReaderSurvey();
        survey.setType(type).setStatus(status).setAnswer(answer);

        return survey;
    }
}
