/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;

// The variable names for this class come from PANDA.

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class WorkListReportDto implements AbstractWorkReportDto {
    public static final String FILENAME = "PANDA_IBLab_Session_Output";
    public static final String[] HEADERS = {
            "sessionId",
            "readerID",
            "sessionDateTimeStart",
            "sessionDateTimeEnd",
            "numberOfReads",
            "numberOfSkips",
            "generalDiscomfortStart",
            "generalDiscomfortEnd",
            "fatigueStart",
            "fatigueEnd",
            "headacheStart",
            "headacheEnd",
            "eyestrainStart",
            "eyestrainEnd",
            "focusingStart",
            "focusingEnd",
            "concentratingStart",
            "concentratingEnd",
            "blurredVisionStart",
            "blurredVisionEnd"
    };

    // Work List ID (Integer value that uniquely identifies a work list)
    private Long sessionID;

    // XNAT username of the Reader
    private String readerID;

    // ISO8601 UTC TIME (e.g., 1994-11-05T13:15:30Z)
    private String sessionDateTimeStart;

    // ISO8601 UTC TIME (e.g., 1994-11-05T13:15:30Z)
    private String sessionDateTimeEnd;

    // # of total work items
    private Integer numberOfReads;

    // # of failed work items
    private Integer numberOfSkips;

    private Integer generalDiscomfortStart;

    private Integer generalDiscomfortEnd;

    private Integer fatigueStart;

    private Integer fatigueEnd;

    private Integer headacheStart;

    private Integer headacheEnd;

    private Integer eyestrainStart;

    private Integer eyestrainEnd;

    private Integer focusingStart;

    private Integer focusingEnd;

    private Integer concentratingStart;

    private Integer concentratingEnd;

    private Integer blurredVisionStart;

    private Integer blurredVisionEnd;
}
