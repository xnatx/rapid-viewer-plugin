/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.plugin;

import org.nrg.framework.annotations.XnatDataModel;
import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import lombok.extern.slf4j.Slf4j;

@XnatPlugin(value = "rapidReaderPlugin", name = "Rapid Reader Plugin", entityPackages = "org.nrg.xnatx.plugins.rapidReader.entities", logConfigurationFile = "org/nrg/xnatx/plugins/rapid-reader-logback.xml"
)
@ComponentScan({"org.nrg.xnatx.plugins.rapidReader.repositories", "org.nrg.xnatx.plugins.rapidReader.rest",
		"org.nrg.xnatx.plugins.rapidReader.services", "org.nrg.xnatx.plugins.rapidReader.services.impl", "org.nrg.xnatx.plugins.rapidReader.docusign"})
@Slf4j
public class RapidReaderPlugin {
	public RapidReaderPlugin() {
		log.info("Creating the RapidReaderPlugin configuration class");
	}

	@Bean
	public String templatePluginMessage() {
		return "This comes from deep within the template plugin.";
	}
}
