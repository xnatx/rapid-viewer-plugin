package org.nrg.xnatx.plugins.rapidReader.docusign.preferences;

public interface AbstractDocusignPreferences {

    String getPrivateKey();

    void setPrivateKey(String privateKey);

    String getIntegrationKey();

    void setIntegrationKey(String integrationKey);

    String getApiUsername();

    void setApiUsername(String apiUsername);

    String getAuthServiceUri();

    void setAuthServiceUri(String authServiceUri);

    Integer getExpiresAfterSec();

    void setExpiresAfterSec(Integer expiresAfterSec);

    String getScope();

    void setScope(String scope);
}
