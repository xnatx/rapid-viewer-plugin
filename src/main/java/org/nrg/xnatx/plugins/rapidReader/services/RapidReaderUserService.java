/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidReaderUser;

public interface RapidReaderUserService extends BaseHibernateService<RapidReaderUser> {
    RapidReaderUser findByXdatUserId(final Integer xdatUserId);
}
