package org.nrg.xnatx.plugins.rapidReader.docusign.helpers;

import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.ApiException;
import com.docusign.esign.model.*;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnatx.plugins.rapidReader.docusign.constants.DocusignConstant;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.AccessTokenResponse;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.UserInfoResponse;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.WorkArguments;
import org.nrg.xnatx.plugins.rapidReader.docusign.exceptions.AccountNotFoundException;
import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.AbstractDocusignPreferences;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.*;

@Slf4j
public class DocusignHelper extends AbstractSignatureHelper {
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER_AUTHENTICATION = "Bearer ";
    private static final String GRANT_TYPE = "urn:ietf:params:oauth:grant-type:jwt-bearer";

    private final AbstractDocusignPreferences docusignPreferences;

    public DocusignHelper(final AbstractDocusignPreferences docusignPreferences) {
        this.docusignPreferences = docusignPreferences;
    }

    public String createJwt() throws Exception {
        String privateKey = docusignPreferences.getPrivateKey();
        if (StringUtils.isBlank(privateKey)) {
            throw new IllegalArgumentException("privateKey is not set to preference.");
        }

        String integrationKey = docusignPreferences.getIntegrationKey();
        if (StringUtils.isBlank(integrationKey)) {
            throw new IllegalArgumentException("integrationKey is not set to preference.");
        }

        String apiUsername = docusignPreferences.getApiUsername();
        if (StringUtils.isBlank(apiUsername)) {
            throw new IllegalArgumentException("apiUsername is not set to preference.");
        }

        String authServiceUri = docusignPreferences.getAuthServiceUri();
        if (StringUtils.isBlank(authServiceUri)) {
            throw new IllegalArgumentException("authServiceUri is not set to preference.");
        }

        String scope = docusignPreferences.getScope();
        if (StringUtils.isBlank(scope)) {
            log.warn("scope is not valid... using signature");
            scope = "signature";
        }

        Integer expiresAfterSec = docusignPreferences.getExpiresAfterSec();
        if (expiresAfterSec <= 0) {
            log.warn("expiresAfterSec is not valid... using 3600");
            expiresAfterSec = 3600;
        }

        log.debug("Loading private key");
        Key key = JwtHelper.loadKey(privateKey);

        log.debug("Constructing Jws [integrationKey={}, apiUsername={}, authServiceUri={}, scope={}, expiresAfterSec={}]",
                integrationKey, apiUsername, authServiceUri, scope, expiresAfterSec);

        return JwtHelper.constructJws(
                key,
                integrationKey,
                apiUsername,
                authServiceUri,
                scope,
                new Date(),
                expiresAfterSec);
    }

    public String obtainAccessToken(String jws) throws IOException {
        Map<String, Object> data = new LinkedHashMap<>();
        data.put("grant_type", GRANT_TYPE);
        data.put("assertion", jws);
        HttpContent content = new UrlEncodedContent(data);

        String url = "https://" + docusignPreferences.getAuthServiceUri() + "/oauth/token";
        log.debug("Calling the HTTP POST method to {} for obtaining accessToken", url);

        HttpRequestFactory requestFactory
                = new NetHttpTransport().createRequestFactory(
                (HttpRequest request) -> request.setParser(new JsonObjectParser(new JacksonFactory())));

        HttpRequest request = requestFactory.buildPostRequest(
                new GenericUrl(url),
                content);

        Type type = new TypeToken<AccessTokenResponse>() {
        }.getType();

        AccessTokenResponse response = (AccessTokenResponse) request.execute().parseAs(type);
        log.debug("AccessToken is retrieved: {}", accessToken);

        this.accessToken = response.getAccessToken();
        return response.getAccessToken();
    }

    public UserInfoResponse getUserInfo() throws IOException {
        HttpRequestFactory requestFactory
                = new NetHttpTransport().createRequestFactory(
                (HttpRequest request) -> request.setParser(new JsonObjectParser(new JacksonFactory())));

        String url = "https://" + docusignPreferences.getAuthServiceUri() + "/oauth/userinfo";
        log.debug("Calling the HTTP get method to {} for retrieving userInfo", url);

        HttpRequest request = requestFactory.buildGetRequest(
                new GenericUrl(url));
        request.getHeaders().setAuthorization("Bearer " + accessToken);
        Type type = new TypeToken<UserInfoResponse>() {
        }.getType();

        UserInfoResponse userInfo = (UserInfoResponse) request.execute().parseAs(type);
        log.debug("userInfo retrieved: {}", userInfo.toString());
        return userInfo;
    }

    public EnvelopeSummary sendEnvelope(WorkArguments args) throws IOException, ApiException {
        UserInfoResponse userInfo = this.getUserInfo();
        List<UserInfoResponse.Account> accounts = userInfo.getAccounts();
        if (accounts.size() == 0) {
            throw new AccountNotFoundException("account not found");
        }
        UserInfoResponse.Account account = accounts.get(0);

        if (!EnvelopeHelper.ENVELOPE_STATUS_CREATED.equalsIgnoreCase(args.getStatus())) {
            args.setStatus(EnvelopeHelper.ENVELOPE_STATUS_SENT);
        }

        EnvelopesApi envelopesApi = createEnvelopesApi(account.getBaseUri() + "/restapi", this.accessToken);

        EnvelopeDefinition envelope = makeEnvelope(args);

        log.debug("Sending the created envelope via the {}", account.getAccountId());

        return envelopesApi.createEnvelope(account.getAccountId(), envelope);
    }

    /**
     * Creates new instance of the eSignature API client.
     *
     * @param basePath        URL to eSignature REST API
     * @param userAccessToken user's access token
     * @return an instance of the {@link ApiClient}
     */
    protected static ApiClient createApiClient(String basePath, String userAccessToken) {
        ApiClient apiClient = new ApiClient(basePath);
        apiClient.addDefaultHeader(AUTHORIZATION, BEARER_AUTHENTICATION + userAccessToken);
        return apiClient;
    }

    /**
     * Creates a new instance of the eSignature EnvelopesApi. This method
     * creates an instance of the ApiClient class silently.
     *
     * @param basePath        URL to eSignature REST API
     * @param userAccessToken user's access token
     * @return an instance of the {@link EnvelopesApi}
     */
    protected static EnvelopesApi createEnvelopesApi(String basePath, String userAccessToken) {
        ApiClient apiClient = createApiClient(basePath, userAccessToken);
        return new EnvelopesApi(apiClient);
    }
}
