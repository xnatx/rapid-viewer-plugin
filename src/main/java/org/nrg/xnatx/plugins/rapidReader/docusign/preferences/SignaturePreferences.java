package org.nrg.xnatx.plugins.rapidReader.docusign.preferences;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.configuration.ConfigPaths;
import org.nrg.prefs.annotations.NrgPreference;
import org.nrg.prefs.annotations.NrgPreferenceBean;
import org.nrg.prefs.beans.AbstractPreferenceBean;
import org.nrg.prefs.exceptions.InvalidPreferenceName;
import org.nrg.prefs.services.NrgPreferenceService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@NrgPreferenceBean(toolId = "rapidReader", toolName = "Rapid Reader Preferences")
@Slf4j
public class SignaturePreferences extends AbstractPreferenceBean implements AbstractDocusignPreferences, AbstractAdobePreferences {
    @Autowired
    public SignaturePreferences(final NrgPreferenceService preferenceService, final ConfigPaths configFolderPaths) {
        super(preferenceService, configFolderPaths);
    }

    private void setValue(String key, String value) {
        try {
            set(value, key);
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name {}: something is very wrong here.", key, e);
        }
    }

    @NrgPreference(defaultValue = "adobe")
    public String getServiceProvider() {
        return getValue("serviceProvider");
    }

    public void setServiceProvider(String serviceProvider) {
        if (serviceProvider.equals("adobe") || serviceProvider.equals("docusign")) {
            throw new IllegalArgumentException("Service Provider should be adobe or docusign");
        }
        setValue("serviceProvider", serviceProvider);
    }

    // Params for DocuSign

    @NrgPreference(defaultValue = "")
    public String getPrivateKey() {
        return getValue("privateKey");
    }

    public void setPrivateKey(String privateKey) {
        try {
            set(privateKey, "privateKey");
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name privateKey: something is very wrong here.", e);
        }
    }

    @NrgPreference(defaultValue = "")
    public String getIntegrationKey() {
        return getValue("integrationKey");
    }

    public void setIntegrationKey(String integrationKey) {
        try {
            set(integrationKey, "integrationKey");
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name integrationKey: something is very wrong here.", e);
        }
    }

    @NrgPreference(defaultValue = "")
    public String getApiUsername() {
        return getValue("apiUsername");
    }

    public void setApiUsername(String apiUsername) {
        try {
            set(apiUsername, "apiUsername");
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name apiUsername: something is very wrong here.", e);
        }
    }

    @NrgPreference(defaultValue = "account-d.docusign.com")
    public String getAuthServiceUri() {
        return getValue("authServiceUri");
    }

    public void setAuthServiceUri(String authServiceUri) {
        try {
            set(authServiceUri, "authServiceUri");
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name authServiceUri: something is very wrong here.", e);
        }
    }

    @NrgPreference(defaultValue = "3600")
    public Integer getExpiresAfterSec() {
        return getIntegerValue("expiresAfterSec");
    }

    public void setExpiresAfterSec(Integer expiresAfterSec) {
        try {
            setIntegerValue(expiresAfterSec, "expiresAfterSec");
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name expiresAfterSec: something is very wrong here.", e);
        }
    }

    @NrgPreference(defaultValue = "signature")
    public String getScope() {
        return getValue("scope");
    }

    public void setScope(String scope) {
        try {
            set(scope, "scope");
        } catch (InvalidPreferenceName e) {
            log.error("Invalid preference name scope: something is very wrong here.", e);
        }
    }

    // Params for Adobe

    @NrgPreference
    public String getCode() {
        return getValue("code");
    }

    public void setCode(String code) {
        setValue("code", code);
    }

    @NrgPreference(defaultValue = "https://api.adobesign.com")
    public String getApiAccessPoint() {
        return getValue("apiAccessPoint");
    }

    public void setApiAccessPoint(String apiAccessPoint) {
        setValue("apiAccessPoint", apiAccessPoint);
    }

    @NrgPreference(defaultValue = "https://secure.adobesign.com")
    public String getWebAccessPoint() {
        return getValue("webAccessPoint");
    }

    public void setWebAccessPoint(String webAccessPoint) {
        setValue("webAccessPoint", webAccessPoint);
    }

    @NrgPreference
    public String getClientId() {
        return getValue("clientId");
    }

    public void setClientId(String clientId) {
        setValue("clientId", clientId);
    }

    @NrgPreference
    public String getClientSecret() {
        return getValue("clientSecret");
    }

    public void setClientSecret(String clientSecret) {
        setValue("clientSecret", clientSecret);
    }


    @NrgPreference
    public String getRefreshToken() {
        return getValue("refreshToken");
    }

    public void setRefreshToken(String refreshToken) {
        setValue("refreshToken", refreshToken);
    }

}
