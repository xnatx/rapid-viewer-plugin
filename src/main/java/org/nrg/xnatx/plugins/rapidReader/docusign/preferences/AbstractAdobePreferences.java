package org.nrg.xnatx.plugins.rapidReader.docusign.preferences;

public interface AbstractAdobePreferences {

    String getCode();

    void setCode(String code);

    String getApiAccessPoint();

    void setApiAccessPoint(String apiAccessPoint);

    String getWebAccessPoint();

    void setWebAccessPoint(String webAccessPoint);

    String getClientId();

    void setClientId(String clientId);

    String getClientSecret();

    void setClientSecret(String clientSecret);

    String getRefreshToken();

    void setRefreshToken(String refreshToken);
}
