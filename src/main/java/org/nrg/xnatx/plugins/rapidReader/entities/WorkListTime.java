/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

;

@Entity
@Table
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class WorkListTime extends AbstractHibernateEntity {
	private static final long serialVersionUID = 1L;

	private WorkList _workList;

	@NotNull
	private Date _beginDate;

	private Date _endDate;

	private Boolean _abnormallyStopped;

	private String _stopReason;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "workListId", nullable = false)
	public WorkList getWorkList() {
		return _workList;
	}

	@Column(columnDefinition="TEXT")
	public String getStopReason() { return _stopReason; }
}
