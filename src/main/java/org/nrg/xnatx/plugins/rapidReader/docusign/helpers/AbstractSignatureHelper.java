package org.nrg.xnatx.plugins.rapidReader.docusign.helpers;

import com.docusign.esign.api.EnvelopesApi;
import com.docusign.esign.client.ApiClient;
import com.docusign.esign.client.ApiException;
import com.docusign.esign.model.*;
import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xnatx.plugins.rapidReader.docusign.constants.DocusignConstant;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.AccessTokenResponse;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.UserInfoResponse;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.WorkArguments;
import org.nrg.xnatx.plugins.rapidReader.docusign.exceptions.AccountNotFoundException;
import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.AbstractDocusignPreferences;

import java.io.IOException;
import java.lang.reflect.Type;
import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.*;

@Slf4j
public class AbstractSignatureHelper {
    protected static final int SIGN_OFFSET_Y = 144;
    protected static final int SIGN_OFFSET_X = 95;

    protected String accessToken;

    public EnvelopeDefinition makeEnvelope(WorkArguments args) throws IOException {
        log.debug("Making an envelope");
        String documentId = "1";
        int pageNumber = 1;

        // The DocuSign platform searches throughout your envelope's documents
        // for matching anchor strings. So the signHere2 tab will be used in
        // both document 2 and 3 since they use the same anchor string for
        // their "signer 1" tabs.
        Tabs signerTabs = EnvelopeHelper.createSignerTabs(
                EnvelopeHelper.createSignHere(documentId, pageNumber, SIGN_OFFSET_Y, SIGN_OFFSET_X));

        // Create a signer recipient to sign the document, identified by name
        // and email. We're setting the parameters via the object creation.
        // RoutingOrder (lower means earlier) determines the order of deliveries
        // to the recipients. Parallel routing order is supported by using the
        // same integer as the order for two or more recipients.
        log.debug("Signed user is {}", args.getSignerEmail());
        Signer signer = new Signer();
        signer.setEmail(args.getSignerEmail());
        signer.setName(args.getSignerName());
        signer.setRecipientId("1");
        signer.setRoutingOrder("1");
        signer.setTabs(signerTabs);

        // create a cc recipient to receive a copy of the documents, identified by name and email
        log.debug("cc recipient is {}", args.getCcEmail());
        CarbonCopy cc = new CarbonCopy();
        cc.setEmail(args.getCcEmail());
        cc.setName(args.getCcName());
        cc.setRecipientId("2");
        cc.setRoutingOrder("2");

        log.debug("Creating an envelope");
        // The order in the docs array determines the order in the envelope
        EnvelopeDefinition envelope = new EnvelopeDefinition();
        envelope.setEmailSubject(DocusignConstant.EMAIL_SUBJECT);
        envelope.setEmailBlurb(DocusignConstant.EMAIL_BODY);
        envelope.setDocuments(Collections.singletonList(
                EnvelopeHelper.createDocumentFromFile(args.getBuffer(), args.getFileName(), args.getDocName(), documentId)));
        envelope.setRecipients(EnvelopeHelper.createRecipients(signer, cc));
        // Request that the envelope be sent by setting |status| to "sent".
        // To request that the envelope be created as a draft, set to "created"
        envelope.setStatus(args.getStatus());
        log.debug(envelope.toString());
        return envelope;
    }
}
