package org.nrg.xnatx.plugins.rapidReader.dto.exceptions;

import org.nrg.xapi.exceptions.NotFoundException;

public class WorkListException extends NotFoundException {
    public WorkListException(String msg) {
        super(msg);
    }
}
