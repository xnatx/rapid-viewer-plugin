/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem.WorkItemStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class WorkItemDto {

	private String projectId;

	private String projectLabel;

	private String subjectId;

	private String subjectLabel;

	private String experimentId;

	private String experimentLabel;

	private WorkItemStatus status;

	private String reportAssessorId;

	public WorkItem toWorkListItem(Long workListId) {
		WorkItem item = new WorkItem();
		item.setProjectId(this.getProjectId()).setProjectLabel(this.getProjectLabel()).setSubjectId(this.getSubjectId())
				.setSubjectLabel(this.getSubjectLabel()).setExperimentId(this.getExperimentId())
				.setExperimentLabel(this.getExperimentLabel()).setStatus(this.getStatus())
				.setReportAssessorId(this.getReportAssessorId());

		WorkList workList = new WorkList();
		workList.setId(workListId);
		item.setWorkList(workList);
		return item;
	}
}
