package org.nrg.xnatx.plugins.rapidReader.services.impl;

import java.util.List;
import java.util.Map;

import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatExperimentdataShare;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.security.UserGroupI;
import org.nrg.xdat.security.helpers.Groups;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.dto.WorkItemDto;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.etc.RapidReaderUserType;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginCode;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginException;
import org.nrg.xnatx.plugins.rapidReader.utils.Security;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PermissionService {

	public void checkPermission(XnatImagesessiondata sessionData, RapidReaderUserType userType, UserI user,
			String projectId, String subjectId, String subjectLabel, String experimentId, String experimentLabel)
			throws PluginException {
		Security.checkProject(user, projectId);

		if (!projectId.equals(sessionData.getProject())) {
			throw new PluginException("Bad Project ID : " + projectId, PluginCode.HttpUnprocessableEntity);
		}
		if (!experimentLabel.equals(sessionData.getLabel())) {
			throw new PluginException("Bad Experiment label : " + experimentLabel, PluginCode.HttpUnprocessableEntity);
		}
		if (!subjectId.equals(sessionData.getSubjectId())) {
			throw new PluginException("Bad Subject ID : " + subjectId, PluginCode.HttpUnprocessableEntity);
		}
		if (!subjectLabel.equals(sessionData.getSubjectData().getLabel())) {
			throw new PluginException("Bad Subject Label : " + subjectLabel, PluginCode.HttpUnprocessableEntity);
		}

		switch (userType) {
		case Assigner:
			Map<String, UserGroupI> userGroups = Groups.getGroupsForUser(user);
			if (userGroups.get(Groups.ALL_DATA_ADMIN_GROUP) == null
					&& userGroups.get(projectId + "_" + Groups.OWNER_GROUP) == null) {
				throw new PluginException("Assigner is not an owner of the project: " + projectId,
						PluginCode.HttpUnprocessableEntity);
			}
			break;
		case Reader:
			Security.checkPermissions(user, sessionData.getXSIType() + "/project", projectId, Security.Read);
			break;
		default:
			break;
		}

		boolean isSessionSharedIntoProject = sessionSharedIntoProject(experimentId, projectId);
		if (!isSessionSharedIntoProject) {
			log.info("Project IDs not equal");
		}
	}

	public void checkPermission(XnatImagesessiondata sessionData, RapidReaderUserType userType, UserI user,
			WorkItem workItem) throws PluginException {
		String projectId = workItem.getProjectId() == null ? sessionData.getProject() : workItem.getProjectId();
		String subjectId = workItem.getSubjectId() == null ? sessionData.getSubjectId() : workItem.getSubjectId();
		String subjectLabel = workItem.getSubjectLabel() == null ? sessionData.getSubjectData().getLabel()
				: workItem.getSubjectLabel();
		String experimentId = workItem.getExperimentId() == null ? sessionData.getId() : workItem.getExperimentId();
		String experimentLabel = workItem.getExperimentLabel() == null ? sessionData.getLabel()
				: workItem.getExperimentLabel();
		this.checkPermission(sessionData, userType, user, projectId, subjectId, subjectLabel, experimentId,
				experimentLabel);
	}

	public void checkPermission(XnatImagesessiondata sessionData, RapidReaderUserType userType, UserI user,
			WorkItemDto dto) throws PluginException {
		String projectId = dto.getProjectId() == null ? sessionData.getProject() : dto.getProjectId();
		String subjectId = dto.getSubjectId() == null ? sessionData.getSubjectId() : dto.getSubjectId();
		String subjectLabel = dto.getSubjectLabel() == null ? sessionData.getSubjectData().getLabel()
				: dto.getSubjectLabel();
		String experimentId = dto.getExperimentId() == null ? sessionData.getId() : dto.getExperimentId();
		String experimentLabel = dto.getExperimentLabel() == null ? sessionData.getLabel() : dto.getExperimentLabel();
		this.checkPermission(sessionData, userType, user, projectId, subjectId, subjectLabel, experimentId,
				experimentLabel);
	}

	private boolean sessionSharedIntoProject(String experimentId, String projectId) throws PluginException {
		log.info("OhifViewerApi::sessionSharedIntoProject(" + experimentId + ", " + projectId + ")");
		XnatExperimentdata expData = null;
		XnatImagesessiondata session = null;
		try {
			expData = XnatExperimentdata.getXnatExperimentdatasById(experimentId, null, false);
			session = (XnatImagesessiondata) expData;
		} catch (Exception ex) {
			log.error("Experiment not found: " + experimentId, ex);
			throw new PluginException("Experiment not found: " + experimentId, PluginCode.HttpUnprocessableEntity);
		}

		if (expData.getProject().equals(projectId)) {
			log.info("Experiment " + experimentId + " belongs to project " + projectId);
			return true;
		}

		List<XnatExperimentdataShare> xnatExperimentdataShareList = session.getSharing_share();
		for (XnatExperimentdataShare share : xnatExperimentdataShareList) {
			log.info("Share project ID: " + share.getProject());
			if (share.getProject().equals(projectId)) {
				return true;
			}
		}
		return false;
	}
}
