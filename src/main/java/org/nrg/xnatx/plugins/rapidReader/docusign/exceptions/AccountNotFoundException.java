package org.nrg.xnatx.plugins.rapidReader.docusign.exceptions;

public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(String msg) {
        super(msg);
    }
}
