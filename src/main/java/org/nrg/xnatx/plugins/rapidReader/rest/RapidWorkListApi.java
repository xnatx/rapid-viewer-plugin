/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.rest;

import static org.junit.Assert.assertEquals;
import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

import java.io.File;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.docusign.esign.model.EnvelopeSummary;
import io.swagger.client.model.agreements.AgreementInfo;
import io.swagger.client.model.transientDocuments.TransientDocumentResponse;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.jdbc.Work;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.exceptions.ResourceAlreadyExistsException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.om.XnatExperimentdata;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.docusign.constants.DocusignConstant;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.WorkArguments;
import org.nrg.xnatx.plugins.rapidReader.docusign.helpers.AdobeHelper;
import org.nrg.xnatx.plugins.rapidReader.docusign.helpers.DocusignHelper;
import org.nrg.xnatx.plugins.rapidReader.docusign.helpers.EnvelopeHelper;
import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.SignaturePreferences;
import org.nrg.xnatx.plugins.rapidReader.dto.*;
import org.nrg.xnatx.plugins.rapidReader.entities.*;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem.WorkItemStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.etc.RapidReaderUserType;
import org.nrg.xnatx.plugins.rapidReader.exceptions.AlreadyExistsException;
import org.nrg.xnatx.plugins.rapidReader.services.*;
import org.nrg.xnatx.plugins.rapidReader.services.impl.PermissionService;
import org.nrg.xnatx.plugins.rapidReader.utils.FileUtils;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

@Api
@XapiRestController
@RequestMapping(value = "/workLists")
@Slf4j
public class RapidWorkListApi extends AbstractXapiRestController {
    @Autowired
    protected RapidWorkListApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
                               final WorkListService workListService, final WorkItemService workItemService,
                               final PermissionService permissionService, ReaderSurveyTemplateService templateService, ReaderSurveyService surveyService, WorkListTimeService workListTimeService, SignaturePreferences docusignPrefs) {
        super(userManagementService, roleHolder);
        _userManagementService = userManagementService;
        _workListService = workListService;
        _workItemService = workItemService;
        _permissionService = permissionService;
        _templateService = templateService;
        _surveyService = surveyService;
        _workListTimeService = workListTimeService;
        _signaturePrefs = docusignPrefs;
    }

    @ApiOperation(value = "Always returns true when this plugin is installed on the XNAT")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkLists successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "check", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public void checkifPluginExists() throws NotFoundException {
        log.trace("/workLists/check [GET] called");
    }

    @ApiOperation(value = "Get User Info")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkLists successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "user", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public UserDto getUserInfo(Principal principal) throws NotFoundException {
        log.trace("/workLists/user [GET] called");

        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        return UserDto.fromUser(user);
    }

    @ApiOperation(value = "Returns a list of workLists of a user.", response = WorkList.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkLists successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public List<WorkList> getEntities(Principal principal,
                                      @ApiParam(value = "Status", required = false) @RequestParam(required = false) final WorkListStatus status,
                                      @ApiParam(value = "WorkListName", required = false) @RequestParam(required = false) final String workListName,
                                      @ApiParam(value = "ReportId", required = false) @RequestParam(required = false) final String reportId,
                                      @ApiParam(value = "Description", required = false) @RequestParam(required = false) final String description,
                                      @ApiParam(value = "Due Date From", required = false) @RequestParam(required = false) final Date dueDateFrom,
                                      @ApiParam(value = "Due Date To", required = false) @RequestParam(required = false) final Date dueDateTo,

                                      @ApiParam(value = "Offset", required = false) @RequestParam(required = false, defaultValue = "0") final Integer offset,
                                      @ApiParam(value = "Limit", required = false) @RequestParam(required = false, defaultValue = "25") final Integer limit,
                                      @ApiParam(value = "OrderBy", required = false) @RequestParam(required = false, defaultValue = "id") final String orderBy,
                                      @ApiParam(value = "Asc", required = false) @RequestParam(required = false, defaultValue = "false") final Boolean isAsc,
                                      @ApiParam(value = "showActiveOnly", required = false) @RequestParam(required = false, defaultValue = "false") final Boolean showActiveOnly)
            throws NotFoundException {
        log.trace("/workLists [GET] called [status={}, offset={}, limit={}, orderBy={}, asc={}]", status, offset, limit,
                orderBy, isAsc);
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerXdatUserId = user.getID();

        return _workListService.getWorkLists(readerXdatUserId, orderBy, isAsc, offset, limit, status, workListName,
                reportId, description, dueDateFrom, dueDateTo, showActiveOnly);
    }

    @ApiOperation(value = "Returns a list of all workLists. (only allowed to Site Admins)", response = WorkList.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkLists successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "/all", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET, restrictTo = Admin)
    public List<WorkList> getAll(Principal principal,
                                 @ApiParam(value = "Username", required = false) @RequestParam(required = false) final String username,
                                 @ApiParam(value = "Status", required = false) @RequestParam(required = false) final WorkListStatus status)
            throws NotFoundException {
        log.trace("/workLists/all [GET] called [status={}]", status);
        return _workListService.getAll();
    }

    @ApiOperation(value = "Retrieves the indicated workList.", notes = "Based on the workList ID, not the primary key ID.", response = WorkList.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public WorkList getEntity(@PathVariable final Long workListId) throws NotFoundException {
        log.trace("/workLists/{} [GET] called", workListId);
        if (!_workListService.exists("id", workListId)) {
            throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
        }
        WorkList workList = _workListService.findById(workListId);

        boolean evaluating = workList.getEvaluating();
        List<WorkListTime> times = workList.getTimes();
        if (evaluating && times.size() > 0) {
            WorkListTime lastTime = times.get(times.size() - 1);
            if (wasIdle(lastTime)) {
                workList.setEvaluating(false);
                _workListService.update(workList);
            }
        }

        return workList;
    }

    private boolean wasIdle(WorkListTime lastTime) {
        long endTs = lastTime.getEndDate().getTime();
        long curTs = new Date().getTime();
        return curTs - endTs > 60 * 1000;
    }

    @ApiOperation(value = "Creates a new workList.", response = WorkList.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public WorkList createWorkList(Principal principal, @RequestBody WorkListDto dto)
            throws PluginException, NotFoundException, ResourceAlreadyExistsException {
        log.trace("/workLists [POST] called: {}", dto.toString());

        // Validate the assigner
        UserI assignerUser = XnatUtil.readUser(_userManagementService, principal.getName());

        UserI readerUser = XnatUtil.readUser(_userManagementService, dto.getReaderUsername());
        WorkList workList = dto.toWorkList(readerUser, assignerUser);
        workList.preCreate();

        // Check if the name already exists
        WorkList existing = _workListService.findByReaderXdatUserIdAndName(readerUser.getID(), dto.getName());
        if (existing != null) {
            throw new ResourceAlreadyExistsException("workList", workList.getName());
        }

        WorkList created = null;
        try {
            created = _workListService.create(workList);
        } catch(Exception e) {
            if (e instanceof ConstraintViolationException) {
                throw new ResourceAlreadyExistsException("workList", workList.getName());
            }
        }
        return created;
    }

    @ApiOperation(value = "Updates the indicated workList.", notes = "Based on primary key ID, not subject or record ID.", response = WorkList.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully updated."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT)
    public WorkList updateWorkList(Principal principal, @PathVariable final Long workListId,
                                   @RequestBody final WorkListDto dto) throws NotFoundException {
        log.trace("/workLists/{} [PUT] called: {}", workListId, dto.toString());
        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
        }

        // Only the original assigner and the Site Admin can edit the worklist.
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        if (workList.getAssignerXdatUserId() != user.getID() && !Roles.isSiteAdmin(user)) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        workList.setName(dto.getName());
        workList.setDescription(dto.getDescription());
        workList.setStatus(dto.getStatus());
        workList.setDueDate(dto.getDueDate());
        workList.setReportId(dto.getReportId());
        workList.setFormDisabledWhenComplete(dto.getFormDisabledWhenComplete());
        workList.setRequiredFieldIds(dto.getRequiredFieldIds());
        if (workList.getStatus() == WorkListStatus.Reviewing || workList.getStatus() == WorkListStatus.Complete) {
            workList.setFinishedDate(new Date());
        }
        workList.setXnatDataTypeName(dto.getXnatDataTypeName());
        workList.preCreate();
        _workListService.update(workList);
        return workList;
    }

    @ApiOperation(value = "Updates the workList status.", response = Long.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully updated."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/status/{status}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT)
    public WorkList updateWorkListStatus(Principal principal, @PathVariable final Long workListId,
                                         @PathVariable final WorkListStatus status) throws NotFoundException {
        log.trace("/workLists/{}/status/{} [PUT] called: {}", workListId, status);
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(user)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }

        if (!Roles.isSiteAdmin(user) && userId != workList.getReaderXdatUserId()
                && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        if (status == workList.getStatus()) {
            return workList;
        }

        workList.setStatus(status);
        _workListService.update(workList);
        return workList;
    }

    @ApiOperation(value = "Deletes the indicated workList.", notes = "Based on primary key ID, not subject or record ID.", response = Long.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully deleted."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.DELETE)
    public long deleteEntity(Principal principal, @PathVariable final Long workListId)
            throws NotFoundException, AccessDeniedException {
        log.trace("/workLists/{} [DELETE] called", workListId);
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(user)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }

        if (!Roles.isSiteAdmin(user) && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        _workListService.delete(workList);
        return workListId;
    }

    @ApiOperation(value = "Returns a list of workItems of a workList.", response = WorkList.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkLists successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public List<WorkItem> getWorkListItems(Principal principal, @PathVariable final Long workListId,
                                           @ApiParam(value = "Status", required = false) @RequestParam(required = false) final WorkItemStatus status)
            throws NotFoundException {
        log.trace("/workLists/{}/items [GET] called [status={}]", workListId, status);
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
        }

        if (!Roles.isSiteAdmin(user) && userId != workList.getReaderXdatUserId()
                && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        if (status == null) {
            return _workItemService.getWorkItems(workListId);
        } else {
            return _workItemService.getWorkItemsByStatus(workListId, status);
        }
    }

    @ApiOperation(value = "Returns a list of workItems of a workList.", response = WorkList.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkLists successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 403, message = "Forbidden"), @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items/validate", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public List<WorkItemValidationDto> validateWorkListItems(Principal principal, @PathVariable final Long workListId)
            throws NotFoundException {
        log.trace("/workLists/{}/items/validate [GET] called", workListId);
        String username = principal.getName();
        UserI user = XnatUtil.readUser(_userManagementService, username);
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
        }

        if (!Roles.isSiteAdmin(user) && userId != workList.getReaderXdatUserId()
                && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        UserI readerUser = XnatUtil.readUser(_userManagementService, workList.getReaderXdatUserId());
        List<WorkItem> items = workList.getItems();
        List<WorkItemValidationDto> itemValidations = new ArrayList<WorkItemValidationDto>();

        for (WorkItem item : items) {
            boolean validated = true;
            String message = null;
            try {
                String experimentId = item.getExperimentId();

                XnatImagesessiondata sessionData = XnatUtil.getImageSessionData(experimentId, readerUser);
                item.setProjectId(sessionData.getProject());
                item.setProjectLabel(sessionData.getProjectName());
                item.setSubjectId(sessionData.getSubjectId());
                item.setSubjectLabel(sessionData.getSubjectData().getLabel());
                item.setExperimentLabel(sessionData.getLabel());

                _permissionService.checkPermission(sessionData, RapidReaderUserType.Reader, readerUser, item);
            } catch (Exception e) {
                validated = false;
                message = e.getMessage();
            }
            itemValidations.add(WorkItemValidationDto.createFromWorkItem(item, validated, message));
        }

        return itemValidations;
    }

    @ApiOperation(value = "Creates a new workItem.", response = WorkItem.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public WorkItem createWorkListItem(Principal principal, @PathVariable Long workListId, @RequestBody WorkItemDto dto)
            throws PluginException, NotFoundException {
        log.trace("/{}/items [POST] called: {}", workListId, dto.toString());
        UserI assignerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer assignerUserId = assignerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(assignerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (!Roles.isSiteAdmin(assignerUser) && assignerUserId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        Integer readerXdatUserId = workList.getReaderXdatUserId();
        UserI readerUser = XnatUtil.readUser(_userManagementService, readerXdatUserId);

        String experimentId = dto.getExperimentId();
        XnatImagesessiondata sessionData = XnatUtil.getImageSessionData(experimentId, assignerUser);

        log.debug("Checking if the session: {} can be added to the work list {} ", dto.toString(), workListId);
        _permissionService.checkPermission(sessionData, RapidReaderUserType.Assigner, assignerUser, dto);
        _permissionService.checkPermission(sessionData, RapidReaderUserType.Reader, readerUser, dto);

        WorkItem workItem = dto.toWorkListItem(workListId);
        workItem.setProjectId(sessionData.getProject());
        workItem.setProjectLabel(sessionData.getProjectName());
        workItem.setSubjectId(sessionData.getSubjectId());
        workItem.setSubjectLabel(sessionData.getSubjectData().getLabel());
        workItem.setExperimentLabel(sessionData.getLabel());
        workItem.preCreate();

        WorkItem created;
        try {
            created = _workItemService.create(workItem);
        } catch (ConstraintViolationException e) {
            throw new AlreadyExistsException("session already exists");
        }
        return created;
    }

    @ApiOperation(value = "Updates the indicated workItem", notes = "Based on primary key ID, not subject or record ID.", response = Long.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully updated."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items/{workItemId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT)
    public long updateWorkListItem(Principal principal, @PathVariable final Long workListId,
                                   @PathVariable final Long workItemId, @RequestBody final WorkItemDto dto)
            throws NotFoundException, PluginException {
        log.trace("/{}/items/{} [PUT] called: {}", workListId, workItemId, dto.toString());
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(user)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }

        final WorkItem existing = _workItemService.findById(workItemId);
        if (existing == null) {
            throw new NotFoundException("No workItem with the ID \"" + workItemId + "\" was found.");
        }

        if (!Roles.isSiteAdmin(user) && userId != workList.getReaderXdatUserId()
                && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        Integer readerXdatUserId = existing.getWorkList().getReaderXdatUserId();
        UserI readerUser = XnatUtil.readUser(_userManagementService, readerXdatUserId);

        String experimentId = dto.getExperimentId();
        XnatImagesessiondata sessionData = XnatUtil.getImageSessionData(experimentId, user);

        log.debug("Checking if the session: {} can be changed to the work list {} ", dto.toString(), workListId);
        _permissionService.checkPermission(sessionData, RapidReaderUserType.Reader, readerUser, dto);

        existing.setProjectLabel(sessionData.getProjectName());
        existing.setSubjectId(sessionData.getSubjectId());
        existing.setSubjectLabel(sessionData.getSubjectData().getLabel());
        existing.setExperimentId(sessionData.getId());
        existing.setExperimentLabel(sessionData.getLabel());
        _workItemService.update(existing);
        return workItemId;
    }

    @ApiOperation(value = "Updates the workItem status.", response = WorkItem.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully updated."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items/{workItemId}/status/{status}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT)
    public WorkItem updateWorkItemStatus(
            Principal principal,
            @PathVariable final Long workListId,
            @PathVariable final Long workItemId,
            @PathVariable final WorkItemStatus status,
            @ApiParam(value = "failReason", required = false) @RequestParam(required = false) final String failReason
    ) throws NotFoundException {
        log.trace("/workLists/{}/items/{}/status/{} [PUT] called: {}", workListId, workItemId, status);
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(user)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (!Roles.isSiteAdmin(user) && userId != workList.getReaderXdatUserId()
                && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        final WorkItem workItem = _workItemService.findById(workItemId);
        if (workItem == null) {
            throw new NotFoundException("No workItem with the ID \"" + workItemId + "\" was found.");
        }

        if (status == workItem.getStatus()) {
            return workItem;
        }

        workItem.setStatus(status);
        workItem.setFailReason(failReason);
        workItem.setUpdated(new Date());
        _workItemService.update(workItem);
        adjustWorkListStatus(workListId);
        return workItem;
    }

    @ApiOperation(value = "Deletes the indicated workItem.", notes = "Based on primary key ID, not subject or record ID.", response = Long.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully deleted."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items/{workItemId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.DELETE)
    public long deleteWorkListItem(Principal principal, @PathVariable final Long workListId,
                                   @PathVariable final Long workItemId) throws NotFoundException {
        log.trace("/{}/items/{} [DELETE] called", workListId, workItemId);
        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer userId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(user)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }

        if (!Roles.isSiteAdmin(user) && userId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        final WorkItem existing = _workItemService.findById(workItemId);
        if (existing == null) {
            throw new NotFoundException("No workItem with the ID \"" + workItemId + "\" was found.");
        }
        _workItemService.delete(existing);
        return workItemId;
    }

    @ApiOperation(value = "Save an assessment.", response = WorkItem.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items/{workItemId}/assessment", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public WorkItem saveAssessment(Principal principal, @PathVariable Long workListId, @PathVariable Long workItemId,
                                   @RequestBody AssessmentDto dto) throws PluginException, NotFoundException, JsonProcessingException {
        log.trace("/{}/items/{}/assessment [POST] called: {}}", workListId, workItemId, dto.toString());

        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerXdatUserId = user.getID();

        final WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(user)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (!Roles.isSiteAdmin(user) && readerXdatUserId != workList.getReaderXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        final WorkItem workItem = getWorkItem(workList, workItemId);
        if (workItem == null) {
            throw new NotFoundException("No workItem with the ID \"" + workItemId + "\" was found.");
        }

        String experimentId = workItem.getExperimentId();
        XnatImagesessiondata sessionData = XnatUtil.getImageSessionData(experimentId, user);

        workItem.setProjectLabel(sessionData.getProjectName());
        workItem.setSubjectId(sessionData.getSubjectId());
        workItem.setSubjectLabel(sessionData.getSubjectData().getLabel());
        workItem.setExperimentId(sessionData.getId());
        workItem.setExperimentLabel(sessionData.getLabel());
        workItem.setStatus(WorkItemStatus.Complete);
        workItem.setReportAssessorId(dto.getAssessorId());
//		workItem.setFieldsJson(dto.serializeFields());
        workItem.setUpdated(new Date());

        List<AssessmentDto.AssessmentField> fields = dto.getFields();
        for (AssessmentDto.AssessmentField field : fields) {
            if (field.getName().equals("year")) {
                workItem.setYear(Integer.parseInt(field.getValue()));
            }
            if (field.getName().equals("month")) {
                workItem.setMonth(Integer.parseInt(field.getValue()));
            }
        }

        _workItemService.update(workItem);

        adjustWorkListStatus(workListId);

        return workItem;
    }

    @ApiOperation(value = "Perform a survey.", response = ReaderSurvey.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Survey successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/surveys/{surveyType}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public ReaderSurvey performSurvey(
            Principal principal,
            @PathVariable Long workListId,
            @PathVariable ReaderSurvey.SurveyType surveyType,
            @RequestBody ReaderSurveyDto dto)
            throws PluginException, NotFoundException {
        log.trace("/{}/surveys/{} [POST] called: {}", workListId, surveyType, dto.toString());
        UserI assignerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerUserId = assignerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(assignerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (!Roles.isSiteAdmin(assignerUser) && readerUserId != workList.getReaderXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        Long templateId = workList.getSurveyTemplateId();

        ReaderSurveyTemplate template;
        try {
            template = _templateService.get(templateId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("template not found");
        }

        Integer readerXdatUserId = workList.getReaderXdatUserId();
        ReaderSurvey survey = dto.toReaderSurvey();
        survey.setWorkList(workList);
        survey.setSurveyTemplate(template);
        survey.setType(surveyType);
        survey.setStatus(ReaderSurvey.SurveyStatus.Complete);
        ReaderSurvey created;
        try {
            created = _surveyService.create(survey);
        } catch (ConstraintViolationException e) {
            throw new AlreadyExistsException("survey already exists");
        }
        return created;
    }

    @ApiOperation(value = "Retrieves the indicated survey.", response = ReaderSurvey.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/surveys/{surveyType}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public ReaderSurvey getSurvey(
            Principal principal,
            @PathVariable final Long workListId,
            @PathVariable final ReaderSurvey.SurveyType surveyType
    ) throws NotFoundException {
        log.trace("/workLists/{}/surveys/{} [GET] called", workListId, surveyType);
        UserI assignerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerUserId = assignerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(assignerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (!Roles.isSiteAdmin(assignerUser) && readerUserId != workList.getReaderXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        return _surveyService.getSurvey(workListId, surveyType);
    }

    @ApiOperation(value = "Begin the time tracking")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully began."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/track", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public void beginTimeTrack(Principal principal, @PathVariable final Long workListId)
            throws PluginException, NotFoundException {
        UserI readerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerUserId = readerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(readerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (readerUserId != workList.getReaderXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        WorkListStatus status = workList.getStatus();
        if (status != WorkListStatus.Open) {
            throw new IllegalStateException("Worklist is not open");
        }

        _workListTimeService.createWorkListTime(workListId);

        if (workList.getEvaluating() == false) {
            workList.setEvaluating(true);
            _workListService.update(workList);
        }
    }

    @ApiOperation(value = "Update the time tracking")
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully update."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/track", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT)
    public void updateTimeTrack(Principal principal, @PathVariable final Long workListId, @RequestBody WorkListTimeDto dto)
            throws PluginException, NotFoundException {
        log.trace("/workLists/{}/track [PUT] called", workListId);
        UserI readerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerUserId = readerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(readerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (readerUserId != workList.getReaderXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        _workListTimeService.updateWorkListTime(dto.getId(), dto.getAbnormallyStopped(), dto.getStopReason());

        if (dto.getAbnormallyStopped()) {
            if (workList.getEvaluating() == true) {
                workList.setEvaluating(false);
                _workListService.update(workList);
            }
        }
    }

    @ApiOperation(value = "Create and send an PDF to reader for signing")
    @ApiResponses({@ApiResponse(code = 200, message = "Envelope successfully sent"),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/sign", produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public void signPdf(Principal principal,
                        @PathVariable final Long workListId,
                        MultipartFile file)
            throws Exception {
        log.trace("/workLists/{}/sign [POST] called", workListId);
        UserI readerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer readerUserId = readerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(readerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (readerUserId != workList.getReaderXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        String serviceProvider = _signaturePrefs.getServiceProvider();
        String envelopeId = null;
        if (serviceProvider.equals("docusign")) {
            log.debug("Signing with docusign");
            workList.setSignatureServiceProvider("docusign");
            EnvelopeSummary summary = createAndSendEnvelope(workList, file, readerUser);
            envelopeId = summary.getEnvelopeId();
        } else {
            log.debug("Signing with adobe");
            workList.setSignatureServiceProvider("adobe");
            AgreementInfo agreementInfo = createAndSendAdobeEnvelope(workList, file, readerUser);
            envelopeId = agreementInfo.getId();
        }

        log.debug("Updating workList {} to Complete", workListId);

        workList.setEnvelopeId(envelopeId);
        workList.setStatus(WorkListStatus.Complete);
        _workListService.update(workList);
    }

    @ApiOperation(value = "Creates all the new workItems in a project.", response = WorkItem.class)
    @ApiResponses({@ApiResponse(code = 200, message = "WorkList successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{workListId}/items/projects/{projectId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST)
    public List<WorkItemResultDto> createWorkListItemsInProject(Principal principal, @PathVariable Long workListId, @PathVariable String projectId)
            throws PluginException, NotFoundException {
        log.trace("/{}/items/projects/{} [POST] called", workListId, projectId);
        UserI assignerUser = XnatUtil.readUser(_userManagementService, principal.getName());
        Integer assignerUserId = assignerUser.getID();

        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            if (Roles.isSiteAdmin(assignerUser)) {
                throw new NotFoundException("No workList with the ID \"" + workListId + "\" was found.");
            } else {
                throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
            }
        }
        if (!Roles.isSiteAdmin(assignerUser) && assignerUserId != workList.getAssignerXdatUserId()) {
            throw new AccessDeniedException("Access to the ID \"" + workListId + "\" is forbidden");
        }

        Integer readerXdatUserId = workList.getReaderXdatUserId();
        UserI readerUser = XnatUtil.readUser(_userManagementService, readerXdatUserId);

        // Read Project
        XnatProjectdata projectData = XnatProjectdata.getProjectByIDorAlias(projectId, readerUser, false);
        if (projectData == null) {
            throw new NotFoundException("No project with the ID \"" + projectId + "\" was found.");
        }

        List<XnatExperimentdata> xnatExperiments = projectData.getExperiments();
        List<WorkItemResultDto> results = new ArrayList<>();
        for (XnatExperimentdata xnatExperiment : xnatExperiments) {
            if (!(xnatExperiment instanceof XnatImagesessiondata)) {
                continue;
            }

            Long workItemId = -1L;
            String failReason = null;
            Boolean status = true;
            try {
                workItemId = createWorkItem(workList, (XnatImagesessiondata) xnatExperiment);
            } catch(Exception e) {
                status = false;
                failReason = e.toString();
            }
            results.add(new WorkItemResultDto(workItemId, xnatExperiment.getId(), status, failReason));
        }

        return results;
    }

    private Long createWorkItem(WorkList workList, XnatImagesessiondata imageSession) {
        log.debug("Creating a workitem for: {} ({})", imageSession.getId(), imageSession.getLabel());

        WorkItem workItem = new WorkItem();
        workItem.setWorkList(workList);
        workItem.setProjectId(imageSession.getProject());
        workItem.setProjectLabel(imageSession.getProjectName());
        workItem.setSubjectId(imageSession.getSubjectId());
        workItem.setSubjectLabel(imageSession.getSubjectData().getLabel());
        workItem.setExperimentId(imageSession.getId());
        workItem.setExperimentLabel(imageSession.getLabel());
        workItem.preCreate();

        WorkItem created;
        try {
            created = _workItemService.create(workItem);
        } catch (ConstraintViolationException e) {
            throw new AlreadyExistsException("session already exists");
        }
        return created.getId();
    }

    private AgreementInfo createAndSendAdobeEnvelope(WorkList workList, MultipartFile uploadedFile, UserI readerUser) throws Exception {
        AdobeHelper adobeHelper = new AdobeHelper(_signaturePrefs);
        adobeHelper.obtainAccessToken();

        Integer assignerUserId = workList.getAssignerXdatUserId();
        UserI assignerUser = XnatUtil.readUser(_userManagementService, assignerUserId);

        WorkArguments args = new WorkArguments();
        args.setStatus(EnvelopeHelper.ENVELOPE_STATUS_SENT);
        args.setSignerEmail(readerUser.getEmail());
        args.setSignerName(readerUser.getFirstname() + " " + readerUser.getLastname());
        args.setCcEmail(assignerUser.getEmail());
        args.setCcName(assignerUser.getFirstname() + " " + assignerUser.getLastname());
        args.setFileName(uploadedFile.getOriginalFilename());
        args.setDocName(DocusignConstant.DOCUMENT_NAME);
        args.setFile(FileUtils.convert(uploadedFile));

        TransientDocumentResponse transientDocumentResponse = adobeHelper.sendTransientDocument(args);
        args.setTransientDocumentId(transientDocumentResponse.getTransientDocumentId());

        return adobeHelper.sendAgreement(args);
    }

    private EnvelopeSummary createAndSendEnvelope(WorkList workList, MultipartFile file, UserI readerUser) throws Exception {
        DocusignHelper docusignHelper = new DocusignHelper(_signaturePrefs);
        String jws = docusignHelper.createJwt();

        docusignHelper.obtainAccessToken(jws);

        Integer assignerUserId = workList.getAssignerXdatUserId();
        UserI assignerUser = XnatUtil.readUser(_userManagementService, assignerUserId);

        WorkArguments args = new WorkArguments();
        args.setStatus(EnvelopeHelper.ENVELOPE_STATUS_SENT);
        args.setSignerEmail(readerUser.getEmail());
        args.setSignerName(readerUser.getFirstname() + " " + readerUser.getLastname());
        args.setCcEmail(assignerUser.getEmail());
        args.setCcName(assignerUser.getFirstname() + " " + assignerUser.getLastname());
        args.setFileName(file.getOriginalFilename());
        args.setDocName(DocusignConstant.DOCUMENT_NAME);
        args.setBuffer(file.getBytes());
        EnvelopeSummary summary = docusignHelper.sendEnvelope(args);

        log.debug("The envelope for the workList {} is successfully created: ", workList.getId(), summary.toString());

        return summary;
    }

    private void adjustWorkListStatus(Long workListId) {
        final WorkList workList = _workListService.findById(workListId);
        List<WorkItem> items = workList.getItems();

        WorkListStatus prevStatus = workList.getStatus();
        WorkListStatus newStatus = prevStatus;

        switch (workList.getStatus()) {
            case Complete:
                if (!isWorkListFinished(items)) {
                    newStatus = WorkListStatus.Open;
                }
                break;

            case Open:
//            case Partial:
            case Reviewing:
                if (isWorkListFinished(items)) {
                    if (workList.getSignRequired() && workList.getEnvelopeId() == null) {
                        newStatus = WorkListStatus.Reviewing;
                    } else {
                        newStatus = WorkListStatus.Complete;
                    }
                } else if (hasFinishedWorkList(items)) {
                    newStatus = WorkListStatus.Open;
                }
                break;

            default:
                break;
        }

        if (newStatus != prevStatus) {
            log.trace("Updating WorkList {} status from {} to {}", workList.getId(), prevStatus, newStatus);
            workList.setStatus(newStatus);
            _workListService.update(workList);
        }
    }

    private boolean hasFinishedWorkList(List<WorkItem> items) {
        for (WorkItem item : items) {
            WorkItemStatus status = item.getStatus();
            if (status == WorkItemStatus.Complete || status == WorkItemStatus.Cancelled) {
                return true;
            }
        }
        return false;
    }

    private boolean isWorkListFinished(List<WorkItem> items) {
        for (WorkItem item : items) {
            WorkItemStatus status = item.getStatus();
            if (status == WorkItemStatus.InProgress || status == WorkItemStatus.Open) {
                return false;
            }
        }
        return true;
    }

    private WorkItem getWorkItem(WorkList workList, Long workItemId) {
        List<WorkItem> items = workList.getItems();

        for (WorkItem item : items) {
            if (item.getId() == workItemId) {
                return item;
            }
        }
        return null;
    }

    private final UserManagementServiceI _userManagementService;
    private final WorkListService _workListService;
    private final WorkItemService _workItemService;
    private final PermissionService _permissionService;
    private final ReaderSurveyTemplateService _templateService;
    private final ReaderSurveyService _surveyService;
    private final WorkListTimeService _workListTimeService;
    private final SignaturePreferences _signaturePrefs;
}
