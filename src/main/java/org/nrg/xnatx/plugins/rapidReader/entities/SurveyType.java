package org.nrg.xnatx.plugins.rapidReader.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.google.common.base.Function;
import com.google.common.collect.Lists;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;

public enum SurveyType {

    OPENING("opening"),
    CLOSING("closing");

    private final String name;

    @JsonCreator
    SurveyType(final String name) {
        this.name = name;
    }

    @JsonValue
    public String getName() {
        return name;
    }

    public static List<String> names() {
        return Lists.transform(Arrays.asList(SurveyType.values()), new Function<SurveyType, String>() {
            @Nullable
            @Override
            public String apply(@Nullable final SurveyType type) {
                return type != null ? type.getName() : "";
            }
        });
    }

}