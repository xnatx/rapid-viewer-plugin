package org.nrg.xnatx.plugins.rapidReader.services;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang3.StringUtils;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.dto.AbstractWorkReportDto;
import org.nrg.xnatx.plugins.rapidReader.dto.WorkItemReportDto;
import org.nrg.xnatx.plugins.rapidReader.dto.WorkListReportDto;
import org.nrg.xnatx.plugins.rapidReader.dto.exceptions.WorkListException;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurvey;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkListTime;
import org.nrg.xnatx.plugins.rapidReader.utils.DateUtils;
import org.nrg.xnatx.plugins.rapidReader.utils.ZipUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ReportService {
    private final UserManagementServiceI _userManagementService;
    private final WorkListService _workListService;
    private final ReaderSurveyService _surveyService;

    @Autowired
    public ReportService(UserManagementServiceI userManagementService, WorkListService workListService, ReaderSurveyService surveyService) {
        _userManagementService = userManagementService;
        _workListService = workListService;
        _surveyService = surveyService;
    }

    public String generateFileNameWithoutExtension(Class<? extends AbstractWorkReportDto> reportDto) {
        String fileType;
        if (reportDto.isAssignableFrom(WorkListReportDto.class)) {
            fileType = WorkListReportDto.FILENAME;
        } else {
            fileType = WorkItemReportDto.FILENAME;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyddMM-HHmmss");
        String dateString = simpleDateFormat.format(new Date());
        return String.format("%s_%s", dateString, fileType);
    }

    public byte[] createZippedWorkListReport(List<Long> workListIds, String fileNameWithoutExtension, Boolean includesHeader, Boolean skipIfNotComplete) throws IOException, NoSuchAlgorithmException, NotFoundException {
        List<WorkListReportDto> dtos = createWorkListReportDtos(workListIds, skipIfNotComplete);

        byte[] reportFileBytes = createWorkListReportCsv(dtos, includesHeader);
        return ZipUtils.zipWithChecksum(reportFileBytes, fileNameWithoutExtension);
    }

    public byte[] createZippedWorkListReport(String projectId, String fileNameWithoutExtension, Boolean includesHeader) throws IOException, NoSuchAlgorithmException, NotFoundException {
        List<Long> workListIds = _workListService.findByProjectId(projectId).stream().map(workList -> workList.getId()).collect(Collectors.toList());
        if (workListIds.size() == 0) {
            throw new NotFoundException(String.format("WorkLists that include the projectId: %s", projectId));
        }
        log.info("{} workLists found from {}: {} not found", workListIds.size(), projectId, StringUtils.join(workListIds, ","));
        return createZippedWorkListReport(workListIds, fileNameWithoutExtension, includesHeader, true);
    }

    private List<WorkListReportDto> createWorkListReportDtos(List<Long> workListIds, Boolean skipIfNotComplete) throws NotFoundException {
        List<WorkListReportDto> dtos = new ArrayList<>();
        for (Long workListId : workListIds) {
            try {
                dtos.add(createWorkListReportDto(workListId));
            } catch(WorkListException e) {
                if (!skipIfNotComplete) {
                    throw e;
                }
                log.warn("WorkList {} skipped: {}", workListId, e.toString());
            }
        }
        return dtos;
    }

    private WorkListReportDto createWorkListReportDto(Long workListId) throws NotFoundException {
        WorkList workList = retrieveWorkList(workListId);
        List<WorkListTime> times = workList.getTimes();

        ReaderSurvey preSurvey = _surveyService.getSurvey(workListId, ReaderSurvey.SurveyType.PreSurvey);
        if (preSurvey == null) {
            throw new WorkListException(String.format("PreSurvey for the WorkList (%d) is not found", workListId));
        }
        String preSurveyAnswer = preSurvey.getAnswer();
        String[] preSurveyAnswers = preSurveyAnswer.split("_\\$_");

        ReaderSurvey postSurvey = _surveyService.getSurvey(workListId, ReaderSurvey.SurveyType.PostSurvey);
        if (postSurvey == null) {
            throw new WorkListException(String.format("PostSurvey for the WorkList (%d) is not found", workListId));
        }
        String postSurveyAnswer = postSurvey.getAnswer();
        String[] postSurveyAnswers = postSurveyAnswer.split("_\\$_");

        List<WorkItem> workItems = workList.getItems();
        int numOfFails = 0;
        for (WorkItem workItem : workItems) {
            if (workItem.getStatus() != WorkItem.WorkItemStatus.Complete) {
                numOfFails++;
            }
        }

        Date beginDate = times.get(0).getBeginDate();
        Date endDate = times.get(times.size() - 1).getEndDate();

        return WorkListReportDto.builder()
                .sessionID(workList.getId())
                .readerID(getUsername(workList.getReaderXdatUserId()))
                .sessionDateTimeStart(DateUtils.toISO8601Format(beginDate))
                .sessionDateTimeEnd(DateUtils.toISO8601Format(endDate))
                .numberOfReads(workItems.size())
                .numberOfSkips(numOfFails)
                .generalDiscomfortStart(Integer.parseInt(preSurveyAnswers[0]) - 1)
                .generalDiscomfortEnd(Integer.parseInt(postSurveyAnswers[0]) - 1)
                .fatigueStart(Integer.parseInt(preSurveyAnswers[1]) - 1)
                .fatigueEnd(Integer.parseInt(postSurveyAnswers[1]) - 1)
                .headacheStart(Integer.parseInt(preSurveyAnswers[2]) - 1)
                .headacheEnd(Integer.parseInt(postSurveyAnswers[2]) - 1)
                .eyestrainStart(Integer.parseInt(preSurveyAnswers[3]) - 1)
                .eyestrainEnd(Integer.parseInt(postSurveyAnswers[3]) - 1)
                .focusingStart(Integer.parseInt(preSurveyAnswers[4]) - 1)
                .focusingEnd(Integer.parseInt(postSurveyAnswers[4]) - 1)
                .concentratingStart(Integer.parseInt(preSurveyAnswers[5]) - 1)
                .concentratingEnd(Integer.parseInt(postSurveyAnswers[5]) - 1)
                .blurredVisionStart(Integer.parseInt(preSurveyAnswers[6]) - 1)
                .blurredVisionEnd(Integer.parseInt(postSurveyAnswers[6]) - 1)
                .build();
    }

    private byte[] createWorkListReportCsv(List<WorkListReportDto> dtos, Boolean includesHeader) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(outputStream);
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(WorkListReportDto.HEADERS).withSkipHeaderRecord(!includesHeader))) {
            for (WorkListReportDto dto : dtos) {
                printer.printRecord(
                        dto.getSessionID(),
                        dto.getReaderID(),
                        dto.getSessionDateTimeStart(),
                        dto.getSessionDateTimeEnd(),
                        dto.getNumberOfReads(),
                        dto.getNumberOfSkips(),
                        dto.getGeneralDiscomfortStart(),
                        dto.getGeneralDiscomfortEnd(),
                        dto.getFatigueStart(),
                        dto.getFatigueEnd(),
                        dto.getHeadacheStart(),
                        dto.getHeadacheEnd(),
                        dto.getEyestrainStart(),
                        dto.getEyestrainEnd(),
                        dto.getFocusingStart(),
                        dto.getFocusingEnd(),
                        dto.getConcentratingStart(),
                        dto.getConcentratingEnd(),
                        dto.getBlurredVisionStart(),
                        dto.getBlurredVisionEnd()
                );
            }
        }
        return outputStream.toByteArray();
    }

    public byte[] createZippedWorkItemReport(List<Long> workListIds, String fileNameWithoutExtension, Boolean includesHeader, Boolean skipIfComplete) throws IOException, NoSuchAlgorithmException, NotFoundException {
        List<WorkItemReportDto> dtos = createWorkItemReportDtos(workListIds, skipIfComplete);

        byte[] reportFileBytes = createWorkItemReportCsv(dtos, includesHeader);
        return ZipUtils.zipWithChecksum(reportFileBytes, fileNameWithoutExtension);
    }

    public byte[] createZippedWorkItemReport(String projectId, String fileNameWithoutExtension, Boolean includesHeader) throws IOException, NoSuchAlgorithmException, NotFoundException {
        List<Long> workListIds = _workListService.findByProjectId(projectId).stream().map(workList -> workList.getId()).collect(Collectors.toList());
        if (workListIds.size() == 0) {
            throw new NotFoundException(String.format("WorkLists that include the projectId: %s", projectId));
        }
        log.info("{} workLists found from {}: {} not found", workListIds.size(), projectId, StringUtils.join(workListIds, ","));
        return createZippedWorkItemReport(workListIds, fileNameWithoutExtension, includesHeader, true);
    }

    private List<WorkItemReportDto> createWorkItemReportDtos(List<Long> workListIds, Boolean skipIfComplete) throws NotFoundException {
        List<WorkItemReportDto> dtos = new ArrayList<>();
        for (Long workListId : workListIds) {
            try {
                dtos.addAll(createWorkItemReportDtos(workListId));
            } catch(WorkListException e) {
                if (!skipIfComplete) {
                    throw e;
                }
                log.warn("WorkList {} skipped: {}", workListId, e.toString());
            }
        }

        // Sort
//        dtos.sort(new Comparator<WorkItemReportDto>() {
//            @Override
//            public int compare(WorkItemReportDto o1, WorkItemReportDto o2) {
//                if (!o1.getReaderID().equals(o2.getReaderID())) {
//                    return o1.getReaderID().compareTo(o2.getReaderID());
//                }
//                return o1.getReadDateTimeEnd().compareTo(o2.getReadDateTimeEnd());
//            }
//        });

        int order = 1;
        String currentReaderId = null;
        for(WorkItemReportDto dto : dtos) {
            if (!dto.getReaderID().equals(currentReaderId)) {
                order = 1;
                currentReaderId = dto.getReaderID();
            }
            dto.setOrderID(order++);
        }

        return dtos;
    }

    private WorkList retrieveWorkList(Long workListId) throws NotFoundException {
        WorkList workList = _workListService.findById(workListId);
        if (workList == null) {
            throw new WorkListException(String.format("WorkList (%d) is not found", workListId));
        }
        if (workList.getStatus() != WorkList.WorkListStatus.Complete) {
            throw new WorkListException(String.format("WorkList (%d) is not completed yet", workListId));
        }

        List<WorkListTime> times = workList.getTimes();
        if (times.size() == 0) {
            throw new WorkListException(String.format("WorkList (%d) doesn't have time information", workListId));
        }
        times.sort(Comparator.comparing(WorkListTime::getBeginDate));
        return workList;
    }

    private List<WorkItemReportDto> createWorkItemReportDtos(Long workListId) throws NotFoundException {
        List<WorkItemReportDto> dtos = new ArrayList<>();

        WorkList workList = retrieveWorkList(workListId);

        ReaderSurvey preSurvey = _surveyService.getSurvey(workListId, ReaderSurvey.SurveyType.PreSurvey);
        if (preSurvey == null) {
            throw new WorkListException(String.format("PreSurvey for the WorkList (%d) is not found", workListId));
        }

        List<WorkListTime> times = workList.getTimes();
        int currentTimeIdx = 0;
        int maxTimeIdx = times.size() - 1;
        WorkListTime currentTime = times.get(currentTimeIdx);
        Date lastItemEndDate = preSurvey.getTimestamp();

        List<WorkItem> workItems = workList.getItems();
        String username = getUsername(workList.getReaderXdatUserId());
//        int order = 1;
        for (WorkItem workItem : workItems) {
            boolean isComplete = workItem.getStatus() == WorkItem.WorkItemStatus.Complete;
            int month = isComplete ? workItem.getYear() * 12 + workItem.getMonth() : 0;

            Date evaluationDate = workItem.getTimestamp();

            while (evaluationDate.after(currentTime.getEndDate())) {
                if (currentTimeIdx >= maxTimeIdx) {
                    throw new IllegalStateException(String.format("Evaluation Date exceeds Time End date at the WorkList (%d)", workListId));
                }
                log.warn(String.format("%dth interruption time found. move to next time", currentTimeIdx + 1));
                currentTime = times.get(++currentTimeIdx);
                lastItemEndDate = currentTime.getBeginDate();
            }

            WorkItemReportDto dto = WorkItemReportDto.builder()
                    .studyID(workItem.getExperimentLabel())
                    .readerID(username)
                    .sessionID(workListId)
//                    .orderID(order)
                    .readDateTimeStart(DateUtils.toISO8601Format(lastItemEndDate))
                    .readDateTimeEnd(DateUtils.toISO8601Format(evaluationDate))
                    .boneAgeRead(month)
                    .readFailFlag(isComplete ? 0 : 1)
                    .readFailReason(workItem.getFailReason())
                    .build();

            dtos.add(dto);
//            order++;
            lastItemEndDate = evaluationDate;
        }

        // detect out-of-order item
        String lastReadDateTimeEnd = null;
        boolean isOutOfOrder = false;
        int outOfOrderIndex = -1;
        for (WorkItemReportDto dto : dtos) {
            if (lastReadDateTimeEnd == null || lastReadDateTimeEnd.compareTo(dto.getReadDateTimeEnd()) <= 0) {
                lastReadDateTimeEnd = dto.getReadDateTimeEnd();
                outOfOrderIndex++;
                continue;
            }

            isOutOfOrder = true;
            break;
        }

        if (isOutOfOrder) {
            WorkItemReportDto dto = dtos.get(outOfOrderIndex);
            WorkItemReportDto next = dtos.get(outOfOrderIndex + 1);
            if (next.getReadDateTimeStart().compareTo(next.getReadDateTimeEnd()) > 0) {
                if (outOfOrderIndex >= 1) {
                    String readDateTimeEnd = dtos.get(outOfOrderIndex - 1).getReadDateTimeEnd();
                    next.setReadDateTimeStart(readDateTimeEnd);
                }
            }

            WorkItemReportDto last = dtos.get(dtos.size() - 1);
            if (last.getReadDateTimeEnd().compareTo(dto.getReadDateTimeStart()) > 0) {
                dto.setReadDateTimeStart(last.getReadDateTimeEnd());
            }
        }

        return dtos;
    }

    private byte[] createWorkItemReportCsv(List<WorkItemReportDto> dtos, Boolean includesHeader) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        OutputStreamWriter out = new OutputStreamWriter(outputStream);
        try (CSVPrinter printer = new CSVPrinter(out, CSVFormat.DEFAULT.withHeader(WorkItemReportDto.HEADERS).withSkipHeaderRecord(!includesHeader))) {
            for (WorkItemReportDto dto : dtos) {
                printer.printRecord(
                        dto.getStudyID(),
                        dto.getReaderID(),
                        dto.getSessionID(),
                        dto.getOrderID(),
                        dto.getReadDateTimeStart(),
                        dto.getReadDateTimeEnd(),
                        dto.getBoneAgeRead(),
                        dto.getReadFailFlag(),
                        dto.getReadFailReason()
                );
            }
        }
        return outputStream.toByteArray();
    }

    private String getUsername(Integer xdatUserId) throws NotFoundException {
        UserI user = XnatUtil.readUser(_userManagementService, xdatUserId);
        return user.getUsername();
    }
}
