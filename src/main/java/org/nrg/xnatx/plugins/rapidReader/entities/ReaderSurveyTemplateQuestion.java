package org.nrg.xnatx.plugins.rapidReader.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@Accessors(prefix = "_", chain = true)
@ToString
public class ReaderSurveyTemplateQuestion extends AbstractHibernateEntity {
    public static enum QuestionType {
        Choice, Text
    }

    @Converter(autoApply = true)
    public static class QuestionTypeConverter implements AttributeConverter<ReaderSurveyTemplateQuestion.QuestionType, String> {

        @Override
        public String convertToDatabaseColumn(ReaderSurveyTemplateQuestion.QuestionType type) {
            if (type == null) {
                return null;
            }
            return type.toString();
        }

        @Override
        public ReaderSurveyTemplateQuestion.QuestionType convertToEntityAttribute(String code) {
            if (code == null) {
                return null;
            }

            for (ReaderSurveyTemplateQuestion.QuestionType type : ReaderSurveyTemplateQuestion.QuestionType.values()) {
                if (code.equals(type.toString())) {
                    return type;
                }
            }

            throw new IllegalArgumentException();
        }
    }

    ReaderSurveyTemplate _readerSurveyTemplate;

    private String _question;

    private QuestionType _type = QuestionType.Choice;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "readerSurveyTemplateId", nullable = false)
    public ReaderSurveyTemplate getReaderSurveyTemplate() {
        return _readerSurveyTemplate;
    }

    @Column(columnDefinition="TEXT")
    public String getQuestion() { return _question; }
}