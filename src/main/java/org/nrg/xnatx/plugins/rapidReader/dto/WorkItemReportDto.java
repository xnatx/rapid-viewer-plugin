/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkListTime;

import java.util.Date;

// The variable names for this class come from PANDA.

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class WorkItemReportDto implements AbstractWorkReportDto {
    public static final String FILENAME = "PANDA_IBLab_Reader_Output";
    public static final String[] HEADERS = {
            "studyID",
            "readerID",
            "sessionID",
            "orderID",
            "readDateTimeStart",
            "readDateTimeEnd",
            "boneAgeRead",
            "readFailFlag",
            "readFailReason",
    };

    // XNAT Experiment ID
    private String studyID;

    // XNAT username of the Reader
    private String readerID;

    // Work List ID (Integer value that uniquely identifies a work list)
    private Long sessionID;

    // The index of the work item in the work list (Begins with 1)
    private Integer orderID;

    // ISO8601 UTC TIME (e.g., 1994-11-05T13:15:30Z)
    private String readDateTimeStart;

    // ISO8601 UTC TIME (e.g., 1994-11-05T13:15:30Z)
    private String readDateTimeEnd;

    // xhbm_work_item.age * 12 + xhbm_work_item.month
    private Integer boneAgeRead;

    // xhbm_work_item.status == “Completed” ? 0 : 1
    private Integer readFailFlag;

    private String readFailReason;
}
