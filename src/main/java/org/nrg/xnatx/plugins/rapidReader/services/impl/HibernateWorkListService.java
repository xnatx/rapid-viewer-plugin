/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.repositories.WorkListRepository;
import org.nrg.xnatx.plugins.rapidReader.services.WorkListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

/**
 * Manages {@link WorkList} data objects in Hibernate.
 */
@Service
@Slf4j
public class HibernateWorkListService extends AbstractHibernateEntityService<WorkList, WorkListRepository>
		implements WorkListService {
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public WorkList findById(final Long id) {
		return getDao().findByUniqueProperty("id", id);
	}

	@Transactional
	@Override
	public List<WorkList> getWorkLists(Integer readerXdatUserId, String orderBy, boolean isAsc, Integer offset,
			Integer limit, WorkListStatus status, String name, String reportId, String description, Date dueDateFrom,
			Date dueDateTo, boolean showActiveOnly) {
		return _dao.getWorkLists(readerXdatUserId, orderBy, isAsc, offset, limit, status, name, reportId, description,
				dueDateFrom, dueDateTo, showActiveOnly);
	}

	@Transactional
	@Override
	public List<WorkList> findByProjectId(String projectId) {
		List<WorkList> workLists = _dao.findAll();
		List<WorkList> filtered = new ArrayList<>();
		for(WorkList workList : workLists) {
			boolean hasProject = workList.getItems().stream().anyMatch(workItem -> projectId.equals(workItem.getProjectId()));
			if (hasProject) {
				filtered.add(workList);
			}
		}
		filtered.sort(new Comparator<WorkList>() {
			@Override
			public int compare(WorkList o1, WorkList o2) {
				return (o1.getId() - o2.getId()) > 0 ? 1: -1;
			}
		});
		return filtered;
	}

	@Transactional
	@Override
	public WorkList findByReaderXdatUserIdAndName(Integer readerXdatUserId, String name) {
		return _dao.findByReaderXdatUserIdAndName(readerXdatUserId, name);
	}

	@Autowired
	private WorkListRepository _dao;
}
