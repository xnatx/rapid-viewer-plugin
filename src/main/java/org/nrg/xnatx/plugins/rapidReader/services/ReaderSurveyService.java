/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurvey;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;

import java.util.Date;
import java.util.List;

public interface ReaderSurveyService extends BaseHibernateService<ReaderSurvey> {
    ReaderSurvey getSurvey(final Long workListId, final ReaderSurvey.SurveyType surveyType);
}
