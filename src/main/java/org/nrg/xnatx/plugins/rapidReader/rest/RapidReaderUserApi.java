/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.rest;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.security.user.exceptions.UserInitException;
import org.nrg.xdat.security.user.exceptions.UserNotFoundException;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.dto.RapidReaderAuthDto;
import org.nrg.xnatx.plugins.rapidReader.dto.UserDto;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidReaderUser;
import org.nrg.xnatx.plugins.rapidReader.services.RapidReaderUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

@Api
@XapiRestController
@RequestMapping(value = "/rapidReader/users")
@Slf4j
public class RapidReaderUserApi extends AbstractXapiRestController {
    @Autowired
    protected RapidReaderUserApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
                                 RapidReaderUserService rapidReaderUserService) {
        super(userManagementService, roleHolder);
        _userManagementService = userManagementService;
        _rapidReaderUserService = rapidReaderUserService;
    }

    private UserI readUser(Integer xdatUserId) throws NotFoundException {
        UserI user = null;
        try {
            user = _userManagementService.getUser(xdatUserId);
        } catch (UserNotFoundException | UserInitException e) {
            new NotFoundException("Cannot find user information for " + xdatUserId);
        }
        return user;
    }

    private UserI readUser(String username) throws NotFoundException {
        UserI user = null;
        try {
            user = _userManagementService.getUser(username);
        } catch (UserNotFoundException | UserInitException e) {
            new NotFoundException("Cannot find user information for " + username);
        }
        return user;
    }

    @ApiOperation(value = "Returns a registered rapid reader users.", response = UserDto.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Users successfully retrieved."),
                @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET, restrictTo = Admin)
    public List<UserDto> getRapidUsers(Principal principal)
            throws NotFoundException {
        log.trace("/rapidReader/users");

        List<RapidReaderUser> rapidReaderUsers = _rapidReaderUserService.getAll();

        List<UserDto> userDtos = new ArrayList<>();
        for (RapidReaderUser rapidReaderUser : rapidReaderUsers) {
            UserI user = null;
            try {
                user = this.readUser(rapidReaderUser.getXdatUserId());
            } catch (NotFoundException e) {
            }
            UserDto userDto;
            if (user != null) {
                userDto = UserDto.fromUser(user);
            } else {
                userDto = new UserDto();
                userDto.setXdatUserId(rapidReaderUser.getXdatUserId());
            }
            userDtos.add(userDto);
        }

        return userDtos;
    }

    @ApiOperation(value = "Returns a rapid user authority object.", response = RapidReaderAuthDto.class)
    @ApiResponses({@ApiResponse(code = 200, message = "Users successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "auth", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public RapidReaderAuthDto isRapidUser(Principal principal)
            throws NotFoundException {
        log.trace("/rapidReader/users/auth");

        UserI user = readUser(principal.getName());

        RapidReaderAuthDto dto = new RapidReaderAuthDto();

        RapidReaderUser rapidReaderUser = _rapidReaderUserService.findByXdatUserId(user.getID());

        dto.setIsAdmin(Roles.isSiteAdmin(user));
        dto.setIsRapidReaderUser(rapidReaderUser != null);

        return dto;
    }

    @ApiOperation(value = "Add a new user.", notes = "Either username or xdatUserId is required.", response = RapidReaderUser.class)
    @ApiResponses({@ApiResponse(code = 200, message = "User successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST, restrictTo = Admin)
    public RapidReaderUser addUser(@RequestBody UserDto dto)
            throws NotFoundException {
        log.trace("/rapidReader/users [POST] called: {}", dto.toString());

        UserI user = null;

        Integer xdatUserId = dto.getXdatUserId();
        String username = dto.getUsername();
        if (xdatUserId != null) {
            user = this.readUser(xdatUserId);
        } else {
            user = this.readUser(username);
        }

        if (user == null) {
            throw new NotFoundException("No user is found.");
        }
        RapidReaderUser rapidReaderUser = new RapidReaderUser();
        rapidReaderUser.setXdatUserId(user.getID());
        return _rapidReaderUserService.create(rapidReaderUser);
    }

    @ApiOperation(value = "Deletes a user.", response = Long.class)
    @ApiResponses({@ApiResponse(code = 200, message = "User successfully deleted."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{username}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.DELETE, restrictTo = Admin)
    public Long deleteUser(@PathVariable final String username) throws NotFoundException {
        log.trace("/rapidReader/users/{} [DELETE] called", username);

        UserI user = this.readUser(username);

        if (user == null) {
            throw new NotFoundException("No user with the username \"" + username + "\" is found.");
        }

        Integer xdatUserId = user.getID();

        final RapidReaderUser rapidReaderUser = _rapidReaderUserService.findByXdatUserId(xdatUserId);
        if (rapidReaderUser == null) {
            throw new NotFoundException("No user with the ID \"" + xdatUserId + "\" was found.");
        }

        _rapidReaderUserService.delete(rapidReaderUser);
        return rapidReaderUser.getId();
    }

    private final UserManagementServiceI _userManagementService;
    private final RapidReaderUserService _rapidReaderUserService;
}
