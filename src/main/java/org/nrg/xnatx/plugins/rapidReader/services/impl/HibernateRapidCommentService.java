/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import java.util.List;

import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment.CommentStatus;
import org.nrg.xnatx.plugins.rapidReader.repositories.RapidCommentRepository;
import org.nrg.xnatx.plugins.rapidReader.services.RapidCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Manages {@link RapidComment} data objects in Hibernate.
 */
@Service
public class HibernateRapidCommentService extends AbstractHibernateEntityService<RapidComment, RapidCommentRepository>
		implements RapidCommentService {

	@Override
	public List<RapidComment> getComments(Long workItemId, String orderBy, boolean isAsc, Integer offset, Integer limit) {
		return _dao.getComments(workItemId, orderBy, isAsc, offset, limit);
	}

	@Override
	public List<RapidComment> getCommentsByStatus(Long workItemId, CommentStatus status, String orderBy, boolean isAsc,
			int offset, int limit) {
		return _dao.getCommentsByStatus(workItemId, status, orderBy, isAsc, offset, limit);
	}

	@Autowired
	private RapidCommentRepository _dao;
}
