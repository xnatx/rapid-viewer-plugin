/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;

import java.util.List;

public interface ReaderSurveyTemplateQuestionService extends BaseHibernateService<ReaderSurveyTemplateQuestion> {
    public List<ReaderSurveyTemplateQuestion> getQuestions(Long templateId);
}
