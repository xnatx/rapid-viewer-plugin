package org.nrg.xnatx.plugins.rapidReader.services.impl;

import javax.mail.MessagingException;

import org.nrg.xdat.XDAT;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailService {
	public void sendEmail(String from, String[] tos, String subject, String message) throws MessagingException {
		log.info("Sending email");

		XDAT.getMailService().sendMessage(from, tos, subject, message);
		log.info("Sent email");
	}
}
