/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.repositories;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ReaderSurveyTemplateQuestionRepository extends AbstractHibernateDAO<ReaderSurveyTemplateQuestion> {
    @Transactional
    public List<ReaderSurveyTemplateQuestion> getQuestions(Long templateId) {
        final Criteria criteria = getSession().createCriteria(getParameterizedType());
        criteria.add(Restrictions.eq("readerSurveyTemplate.id", templateId));
        criteria.add(Restrictions.eq("enabled", true));
        criteria.addOrder(Order.asc("id"));
        return criteria.list();
    }
}
