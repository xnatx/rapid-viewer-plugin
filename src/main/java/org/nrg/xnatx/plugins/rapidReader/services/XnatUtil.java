package org.nrg.xnatx.plugins.rapidReader.services;

import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xdat.om.XnatImagesessiondata;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.security.user.exceptions.UserInitException;
import org.nrg.xdat.security.user.exceptions.UserNotFoundException;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginCode;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginException;

public class XnatUtil {
	private XnatUtil() {
	}

	public static UserI readUser(UserManagementServiceI _userManagementService, String username)
			throws NotFoundException {
		UserI user = null;
		try {
			user = _userManagementService.getUser(username);
		} catch (UserNotFoundException | UserInitException e) {
			new NotFoundException("Cannot find user information for " + username);
		}
		return user;
	}

	public static UserI readUser(UserManagementServiceI _userManagementService, Integer xdatUserId)
			throws NotFoundException {
		UserI user = null;
		try {
			user = _userManagementService.getUser(xdatUserId);
		} catch (UserNotFoundException | UserInitException e) {
			new NotFoundException("Cannot find user information for xdat-user-id " + xdatUserId);
		}
		return user;
	}

	public static XnatImagesessiondata getImageSessionData(String id, UserI user) throws PluginException {
		XnatImagesessiondata sessionData = XnatImagesessiondata.getXnatImagesessiondatasById(id, user, false);
		if (sessionData == null) {
			throw new PluginException("No session found for ID: " + id, PluginCode.HttpUnprocessableEntity);
		}
		return sessionData;
	}
}
