/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.repositories.ReaderSurveyTemplateQuestionRepository;
import org.nrg.xnatx.plugins.rapidReader.repositories.ReaderSurveyTemplateRepository;
import org.nrg.xnatx.plugins.rapidReader.services.ReaderSurveyTemplateQuestionService;
import org.nrg.xnatx.plugins.rapidReader.services.ReaderSurveyTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Manages {@link WorkList} data objects in Hibernate.
 */
@Service
@Slf4j
public class HibernateSurveyTemplateQuestionService extends AbstractHibernateEntityService<ReaderSurveyTemplateQuestion, ReaderSurveyTemplateQuestionRepository>
        implements ReaderSurveyTemplateQuestionService {

    @Transactional
    @Override
    public List<ReaderSurveyTemplateQuestion> getQuestions(Long templateId) {
        return _dao.getQuestions(templateId);
    }

    @Autowired
    private ReaderSurveyTemplateQuestionRepository _dao;
}
