/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class WorkListDto {

    private Long id;

    private String readerUsername;

    private String assignerUsername;

    private String name;

    private String description;

    private WorkListStatus status;

    private Date dueDate;

    private String reportId;

    @Builder.Default
    private Boolean formDisabledWhenComplete = true;

    @Builder.Default
    private Boolean signRequired = false;

    private String requiredFieldIds;

    private Long surveyTemplateId;

    private Date finishedDate;

    private String xnatDataTypeName;

    public WorkList toWorkList(UserI reader, UserI assigner) {
        WorkList workList = new WorkList();

        if (StringUtils.isEmpty(xnatDataTypeName)) {
            xnatDataTypeName = reportId;
        }

        workList.setReaderXdatUserId(reader.getID()).setAssignerXdatUserId(assigner.getID()).setName(name)
                .setDescription(description).setStatus(status).setDueDate(dueDate).setReportId(reportId)
                .setFormDisabledWhenComplete(formDisabledWhenComplete).setSignRequired(signRequired).setRequiredFieldIds(requiredFieldIds)
                .setSurveyTemplateId(surveyTemplateId).setXnatDataTypeName(xnatDataTypeName);
        if (id != null) {
            workList.setId(id);
        }

        return workList;
    }
}
