/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.repositories;

import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkListTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public class WorkListTimeRepository extends AbstractHibernateDAO<WorkListTime> {
	@SuppressWarnings("unchecked")
	@Transactional
	public List<WorkListTime> getWorkListTimes(Long workListId) {
		final Criteria criteria = getSession().createCriteria(getParameterizedType());
		criteria.add(Restrictions.eq("workList.id", workListId));
		criteria.addOrder(Order.asc("id"));
		return criteria.list();
	}
}
