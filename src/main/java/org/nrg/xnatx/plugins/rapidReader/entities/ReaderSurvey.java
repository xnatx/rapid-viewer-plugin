package org.nrg.xnatx.plugins.rapidReader.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"workListId", "type"})})
@NoArgsConstructor
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class ReaderSurvey extends AbstractHibernateEntity {
    public static final String SEPARATOR = "_$_";

    private static final long serialVersionUID = 1L;

    public static enum SurveyType {
        PreSurvey, PostSurvey
    }

    public static enum SurveyStatus {
        InProgress, Complete, Cancelled
    }

    @Converter(autoApply = true)
    public static class SurveyTypeConverter implements AttributeConverter<ReaderSurvey.SurveyType, String> {

        @Override
        public String convertToDatabaseColumn(ReaderSurvey.SurveyType type) {
            if (type == null) {
                return null;
            }
            return type.toString();
        }

        @Override
        public ReaderSurvey.SurveyType convertToEntityAttribute(String code) {
            if (code == null) {
                return null;
            }

            for (ReaderSurvey.SurveyType status : ReaderSurvey.SurveyType.values()) {
                if (code.equals(status.toString())) {
                    return status;
                }
            }

            throw new IllegalArgumentException();
        }
    }

    @Converter(autoApply = true)
    public static class SurveyStatusConverter implements AttributeConverter<ReaderSurvey.SurveyStatus, String> {

        @Override
        public String convertToDatabaseColumn(ReaderSurvey.SurveyStatus status) {
            if (status == null) {
                return null;
            }
            return status.toString();
        }

        @Override
        public ReaderSurvey.SurveyStatus convertToEntityAttribute(String code) {
            if (code == null) {
                return null;
            }

            for (ReaderSurvey.SurveyStatus status : ReaderSurvey.SurveyStatus.values()) {
                if (code.equals(status.toString())) {
                    return status;
                }
            }

            throw new IllegalArgumentException();
        }
    }

    private WorkList _workList;

    private ReaderSurveyTemplate _surveyTemplate;

    private SurveyType _type;

    private SurveyStatus _status;

    private String _answer;

    @ManyToOne
    @JoinColumn(name = "workListId", nullable = false)
    public WorkList getWorkList() {
        return _workList;
    }

    @ManyToOne
    @JoinColumn(name = "surveyTemplateId", nullable = false)
    public ReaderSurveyTemplate getSurveyTemplate() {
        return _surveyTemplate;
    }

    @Column(columnDefinition="TEXT")
    public String getAnswer() { return _answer; }
}
