/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.rest;

import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.security.user.exceptions.UserInitException;
import org.nrg.xdat.security.user.exceptions.UserNotFoundException;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.dto.RapidCommentDto;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidComment.CommentStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkItem;
import org.nrg.xnatx.plugins.rapidReader.etc.RapidViewerConstants;
import org.nrg.xnatx.plugins.rapidReader.services.RapidCommentService;
import org.nrg.xnatx.plugins.rapidReader.services.WorkItemService;
import org.nrg.xnatx.plugins.rapidReader.services.impl.EmailService;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Api
@XapiRestController
@RequestMapping(value = "/rapidReader/comments")
@Slf4j
public class RapidCommentApi extends AbstractXapiRestController {
	@Autowired
	protected RapidCommentApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
			final RapidCommentService commentService, final WorkItemService workItemService,
			final EmailService emailService) {
		super(userManagementService, roleHolder);
		_userManagementService = userManagementService;
		_commentService = commentService;
		_workItemService = workItemService;
		_emailService = emailService;
	}

	private UserI readUser(String username) throws NotFoundException {
		UserI user = null;
		try {
			user = _userManagementService.getUser(username);
		} catch (UserNotFoundException | UserInitException e) {
			new NotFoundException("Cannot find user information for " + username);
		}
		return user;
	}

	@ApiOperation(value = "Returns a list of Comments of a user.", response = RapidComment.class, responseContainer = "List")
	@ApiResponses({ @ApiResponse(code = 200, message = "Comments successfully retrieved."),
			@ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
			@ApiResponse(code = 500, message = "Unexpected error") })
	@XapiRequestMapping(value = "{workItemId}/all", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.GET)
	public List<RapidComment> getEntities(Principal principal, @PathVariable final Long workItemId,
			@ApiParam(value = "Status", required = false) @RequestParam(required = false) final CommentStatus status,
			@ApiParam(value = "Offset", required = false) @RequestParam(required = false, defaultValue = "0") final Integer offset,
			@ApiParam(value = "Limit", required = false) @RequestParam(required = false, defaultValue = "25") final Integer limit,
			@ApiParam(value = "OrderBy", required = false) @RequestParam(required = false, defaultValue = "id") final String orderBy,
			@ApiParam(value = "Asc", required = false) @RequestParam(required = false, defaultValue = "false") final Boolean isAsc)
			throws NotFoundException {
		log.trace("/rapidComments [GET] called [status={}, offset={}, limit={}, orderBy={}, asc={}]", status, offset,
				limit, orderBy, isAsc);
		UserI user = readUser(principal.getName());
		Integer ownerXdatUserId = user.getID();

		final WorkItem workItem = _workItemService.findById(workItemId);
		if (workItem == null) {
			if (Roles.isSiteAdmin(user)) {
				throw new NotFoundException("No workItem with the ID \"" + workItemId + "\" was found.");
			} else {
				throw new AccessDeniedException("Access to the ID \"" + workItemId + "\" is forbidden");
			}
		}

		if (!Roles.isSiteAdmin(user) && ownerXdatUserId != workItem.getWorkList().getReaderXdatUserId()) {
			throw new AccessDeniedException("Access to the ID \"" + workItemId + "\" is forbidden");
		}

		if (status == null) {
			return _commentService.getComments(workItemId, orderBy, isAsc, offset, limit);
		} else {
			return _commentService.getCommentsByStatus(workItemId, status, orderBy, isAsc, offset, limit);
		}
	}

	@ApiOperation(value = "Retrieves the indicated Comment.", notes = "Based on the Comment ID, not the primary key ID.", response = RapidComment.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Comment successfully retrieved."),
			@ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
			@ApiResponse(code = 500, message = "Unexpected error") })
	@XapiRequestMapping(value = "{commentId}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.GET)
	public RapidComment getEntity(Principal principal, @PathVariable final Long commentId) throws NotFoundException {
		log.trace("/rapidComments/{} [GET] called", commentId);

		UserI user = readUser(principal.getName());
		Integer ownerXdatUserId = user.getID();

		RapidComment comment = _commentService.retrieve(commentId);
		if (comment == null) {
			if (Roles.isSiteAdmin(user)) {
				throw new NotFoundException("No comment with the ID \"" + commentId + "\" was found.");
			} else {
				throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
			}
		}

		if (!Roles.isSiteAdmin(user) && ownerXdatUserId != comment.getWorkItem().getWorkList().getReaderXdatUserId()) {
			throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
		}

		return comment;
	}

	@ApiOperation(value = "Creates a new Comment.", response = RapidComment.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Comment successfully created."),
			@ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
			@ApiResponse(code = 500, message = "Unexpected error") })
	@XapiRequestMapping(produces = { MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.POST)
	public RapidComment createComment(Principal principal, @RequestBody RapidCommentDto dto)
			throws PluginException, NotFoundException {
		log.trace("/rapidComments [POST] called: {}", dto.toString());

		UserI user = readUser(principal.getName());
		Integer ownerXdatUserId = user.getID();

		Long workItemId = dto.getWorkItemId();
		final WorkItem workItem = _workItemService.retrieve(workItemId);
		if (workItem == null) {
			if (Roles.isSiteAdmin(user)) {
				throw new NotFoundException("No workItem with the ID \"" + workItemId + "\" was found.");
			} else {
				throw new AccessDeniedException("Access to the ID \"" + workItemId + "\" is forbidden");
			}
		}

		if (!Roles.isSiteAdmin(user) && ownerXdatUserId != workItem.getWorkList().getReaderXdatUserId()) {
			throw new AccessDeniedException("Access to the ID \"" + workItemId + "\" is forbidden");
		}
		RapidComment comment = dto.toComment(workItem);
		comment.preCreate();

		_commentService.create(comment);

		try {
			_emailService.sendEmail(user.getEmail(), adjustEmails(comment.getToEmails()).toArray(new String[0]),
					comment.getSubject(), comment.getBody());
			comment.setStatus(CommentStatus.Sent);
		} catch (Exception e) {
			comment.setStatus(CommentStatus.Cancelled);
		}
		_commentService.update(comment);
		return comment;
	}

	private Set<String> adjustEmails(Set<String> emails) {
		for (String groupName : RapidViewerConstants.SPECIAL_GROUP_EMAILS.keySet()) {
			if (emails.contains(groupName)) {
				emails.remove(groupName);
				String[] addedEmails = RapidViewerConstants.SPECIAL_GROUP_EMAILS.get(groupName);
				for (String addedEmail : addedEmails) {
					emails.add(addedEmail);
				}
			}
		}
		return emails;
	}

	@ApiOperation(value = "Updates the indicated Comment.", notes = "Based on primary key ID, not subject or record ID.", response = RapidComment.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Comment successfully updated."),
			@ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
			@ApiResponse(code = 500, message = "Unexpected error") })
	@XapiRequestMapping(value = "{commentId}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.PUT, restrictTo = Admin)
	public RapidComment updateComment(Principal principal, @PathVariable final Long commentId,
			@RequestBody final RapidCommentDto dto) throws NotFoundException {
		log.trace("/rapidComments/{} [PUT] called: {}", commentId, dto.toString());
		UserI user = readUser(principal.getName());
		Integer ownerXdatUserId = user.getID();

		final RapidComment comment = _commentService.retrieve(commentId);
		if (comment == null) {
			if (Roles.isSiteAdmin(user)) {
				throw new NotFoundException("No comment with the ID \"" + commentId + "\" was found.");
			} else {
				throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
			}
		}

		if (!Roles.isSiteAdmin(user) && ownerXdatUserId != comment.getWorkItem().getWorkList().getReaderXdatUserId()) {
			throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
		}

		comment.setToEmails(dto.getToEmails());
		comment.setSubject(dto.getSubject());
		comment.setBody(dto.getBody());
		comment.setStatus(dto.getStatus());
		comment.preCreate();
		_commentService.update(comment);
		return comment;
	}

	@ApiOperation(value = "Updates the Comment status.", response = Long.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Comment successfully updated."),
			@ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
			@ApiResponse(code = 500, message = "Unexpected error") })
	@XapiRequestMapping(value = "{commentId}/status/{status}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.PUT)
	public RapidComment updateCommentStatus(Principal principal, @PathVariable final Long commentId,
			@PathVariable final CommentStatus status) throws NotFoundException {
		log.trace("/Comments/{}/status/{} [PUT] called: {}", commentId, status);
		UserI user = readUser(principal.getName());
		Integer ownerXdatUserId = user.getID();

		final RapidComment comment = _commentService.retrieve(commentId);
		if (comment == null) {
			if (Roles.isSiteAdmin(user)) {
				throw new NotFoundException("No comment with the ID \"" + commentId + "\" was found.");
			} else {
				throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
			}
		}

		if (!Roles.isSiteAdmin(user) && ownerXdatUserId != comment.getWorkItem().getWorkList().getReaderXdatUserId()) {
			throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
		}

		comment.setStatus(status);
		_commentService.update(comment);
		return comment;
	}

	@ApiOperation(value = "Deletes the indicated Comment.", notes = "Based on primary key ID, not subject or record ID.", response = Long.class)
	@ApiResponses({ @ApiResponse(code = 200, message = "Comment successfully deleted."),
			@ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
			@ApiResponse(code = 500, message = "Unexpected error") })
	@XapiRequestMapping(value = "{commentId}", produces = {
			MediaType.APPLICATION_JSON_VALUE }, method = RequestMethod.DELETE, restrictTo = Admin)
	public long deleteComment(Principal principal, @PathVariable final Long commentId) throws NotFoundException {
		log.trace("/rapidComments/{} [DELETE] called", commentId);
		UserI user = readUser(principal.getName());
		Integer ownerXdatUserId = user.getID();

		final RapidComment comment = _commentService.retrieve(commentId);
		if (comment == null) {
			if (Roles.isSiteAdmin(user)) {
				throw new NotFoundException("No comment with the ID \"" + commentId + "\" was found.");
			} else {
				throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
			}
		}

		if (!Roles.isSiteAdmin(user) && ownerXdatUserId != comment.getWorkItem().getWorkList().getReaderXdatUserId()) {
			throw new AccessDeniedException("Access to the ID \"" + commentId + "\" is forbidden");
		}

		_commentService.delete(comment);
		return commentId;
	}

	private final UserManagementServiceI _userManagementService;
	private final RapidCommentService _commentService;
	private final WorkItemService _workItemService;
	private final EmailService _emailService;
}
