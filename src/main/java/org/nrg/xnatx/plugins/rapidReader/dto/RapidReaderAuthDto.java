/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;
import org.nrg.xft.security.UserI;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class RapidReaderAuthDto {
	private Boolean isAdmin;
	private Boolean isRapidReaderUser;
}
