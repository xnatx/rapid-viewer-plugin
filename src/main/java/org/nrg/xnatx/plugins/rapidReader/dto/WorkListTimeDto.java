/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkListTime;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class WorkListTimeDto {

    private Long id;

    private Date beginDate;

    private Date endDate;

    private Boolean abnormallyStopped;

    private String stopReason;

    public WorkListTime toWorkListTime() {
        WorkListTime workListTime = new WorkListTime();
        workListTime.setBeginDate(beginDate).setEndDate(endDate).setAbnormallyStopped(abnormallyStopped).setStopReason(stopReason);
        if (id != null) {
            workListTime.setId(id);
        }

        return workListTime;
    }
}
