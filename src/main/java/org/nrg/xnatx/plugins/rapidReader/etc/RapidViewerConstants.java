package org.nrg.xnatx.plugins.rapidReader.etc;

import java.util.HashMap;
import java.util.Map;

public class RapidViewerConstants {
	private RapidViewerConstants() {
	}

	public static final String TECH_EMAIL_GROUP = "[TECH]";

	public static final String[] TECH_EMAILS = { "wcho24@wustl.edu" };

	public static Map<String, String[]> SPECIAL_GROUP_EMAILS = new HashMap<>();

	static {
		SPECIAL_GROUP_EMAILS.put(TECH_EMAIL_GROUP, TECH_EMAILS);
	}
}
