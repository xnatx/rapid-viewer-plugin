package org.nrg.xnatx.plugins.rapidReader.docusign.helpers;

import com.google.api.client.http.*;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.reflect.TypeToken;
import io.swagger.client.api.AgreementsApi;
import io.swagger.client.api.BaseUrisApi;
import io.swagger.client.api.TransientDocumentsApi;
import io.swagger.client.model.ApiClient;
import io.swagger.client.model.agreements.*;
import io.swagger.client.model.baseUris.BaseUriInfo;
import io.swagger.client.model.transientDocuments.TransientDocumentResponse;
import lombok.extern.slf4j.Slf4j;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.AccessTokenResponse;
import org.nrg.xnatx.plugins.rapidReader.docusign.dto.WorkArguments;
import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.AbstractAdobePreferences;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

@Slf4j
public class AdobeHelper extends AbstractSignatureHelper {
    private static final String GRANT_TYPE = "refresh_token";

    private final AbstractAdobePreferences adobePreferences;

    public AdobeHelper(final AbstractAdobePreferences adobePreferences) {
        this.adobePreferences = adobePreferences;
    }

    public String obtainAccessToken() throws IOException {
        String refreshToken = adobePreferences.getRefreshToken();
        String clientId = adobePreferences.getClientId();
        String clientSecret = adobePreferences.getClientSecret();

        Map<String, Object> data = new LinkedHashMap<>();
        data.put("refresh_token", refreshToken);
        data.put("client_id", clientId);
        data.put("client_secret", clientSecret);
        data.put("grant_type", GRANT_TYPE);
        HttpContent content = new UrlEncodedContent(data);

        String url = "https://" + adobePreferences.getApiAccessPoint() + "/oauth/refresh";
        log.debug("Calling the HTTP POST method to {} for obtaining accessToken [refreshToken={}, clientId={}, clientSecret={}]", url, refreshToken, clientId, clientSecret);

        HttpRequestFactory requestFactory
                = new NetHttpTransport().createRequestFactory(
                (HttpRequest request) -> request.setParser(new JsonObjectParser(new JacksonFactory())));

        HttpRequest request = requestFactory.buildPostRequest(
                new GenericUrl(url),
                content);

        Type type = new TypeToken<AccessTokenResponse>() {
        }.getType();

        AccessTokenResponse response = (AccessTokenResponse) request.execute().parseAs(type);
        log.debug("AccessToken is retrieved: {}", accessToken);

        this.accessToken = response.getAccessToken();
        return response.getAccessToken();
    }

    private ApiClient createApiClient() {
        ApiClient apiClient = new ApiClient();
        String baseUrl = "https://" + adobePreferences.getApiAccessPoint() + "/api/rest/v6";
        apiClient.setBasePath(baseUrl);
        log.debug("Base url for posting transientDocument is", baseUrl);
        return apiClient;
    }

    private String getAuthorization() {
        return "Bearer " + accessToken;
    }

    /**
     * Uploads a transient document to the Adobe Sign server, typically as part of an agreement sending workflow. A "transient document" is a
     * temporary copy of the uploaded document which is available for a limited time after which it is deleted automatically.
     *
     * @param accessToken Access token of the API user.
     * @param file        Name of the file to be uploaded. The file must exist in the "requests" sub-package.
     *                    of characters in the ASCII character given this basic sample implementation.
     * @return JSON response containing the ID of the uploaded transient document.
     */
    public TransientDocumentResponse sendTransientDocument(WorkArguments args) throws Exception {
        ApiClient apiClient = createApiClient();
        String authorization = getAuthorization();
        String mimeType = "application/pdf";
        String xApiUser = null;
        String xOnBehalfOfUser = null;
        String fileName = args.getDocName();
        File file = args.getFile();

        log.debug("Creating a Transient Document with the file {}", file.getName());
        TransientDocumentsApi transientDocumentsApi = new TransientDocumentsApi(apiClient);
        TransientDocumentResponse response = transientDocumentsApi.createTransientDocument(authorization, file, xApiUser, xOnBehalfOfUser, fileName, mimeType);

        log.debug("Transient Document has ben created: {}", response.getTransientDocumentId());

        return response;
    }

    public AgreementInfo sendAgreement(WorkArguments args) throws Exception {
        ApiClient apiClient = createApiClient();
        String authorization = getAuthorization();
        String xApiUser = null;
        String xOnBehalfOfUser = null;
        String transientDocumentId = args.getTransientDocumentId();
        //prepare request body for agreement creation.
        AgreementCreationInfo agreementInfo = new AgreementCreationInfo();
        agreementInfo.setName(args.getDocName());
        agreementInfo.setSignatureType(AgreementCreationInfo.SignatureTypeEnum.ESIGN);
        agreementInfo.setState(AgreementCreationInfo.StateEnum.IN_PROCESS);

        FileInfo fileInfo = new FileInfo();
        fileInfo.setTransientDocumentId(transientDocumentId);
        agreementInfo.addFileInfosItem(fileInfo);

        ParticipantSetInfo signerInfo = new ParticipantSetInfo();
        ParticipantSetMemberInfo signerSetMemberInfo = new ParticipantSetMemberInfo();
        //TODO : Provide email of recipient to whom agreement will be sent
        signerSetMemberInfo.setEmail(args.getSignerEmail());
        signerInfo.addMemberInfosItem(signerSetMemberInfo);
        signerInfo.setName(args.getSignerName());
        signerInfo.setOrder(1);
        signerInfo.setRole(ParticipantSetInfo.RoleEnum.SIGNER);
        agreementInfo.addParticipantSetsInfoItem(signerInfo);

//        ParticipantSetInfo ccerInfo = new ParticipantSetInfo();
//        ParticipantSetMemberInfo ccerSetMemberInfo = new ParticipantSetMemberInfo();
//        ccerSetMemberInfo.setEmail(args.getCcEmail());
//        ccerInfo.setName(args.getCcName());
//        ccerInfo.addMemberInfosItem(ccerSetMemberInfo);
//        ccerInfo.setOrder(2);
//        ccerInfo.setRole(ParticipantSetInfo.RoleEnum.ACCEPTOR);
//        agreementInfo.addParticipantSetsInfoItem(ccerInfo);

        //Create agreement using the transient document.
        log.debug("Creating agreement for the document {}", transientDocumentId);
        AgreementsApi agreementsApi = new AgreementsApi(apiClient);
        AgreementCreationResponse agreementCreationResponse = agreementsApi.createAgreement(authorization, agreementInfo, xApiUser, xOnBehalfOfUser);
        String agreementId = agreementCreationResponse.getId();
        log.debug("Agreement has been created: {}", agreementId);

        //Get agreement info using the agreement id.
        log.debug("Retrieving agreement for {}", agreementId);
        String ifNoneMatch = null;
        AgreementInfo resInfo = agreementsApi.getAgreementInfo(authorization, agreementId, xApiUser, xOnBehalfOfUser, ifNoneMatch);
        log.debug("Status of the agreement {} is {}", agreementId, resInfo.getStatus());

        return resInfo;
    }
}
