/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;;

@Entity
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class RapidComment extends AbstractHibernateEntity {
	private static final long serialVersionUID = 1L;

	public static enum CommentStatus {
		Ready, Sending, Sent, Cancelled 
	}

	@Converter(autoApply = true)
	public static class CommentStatusConverter implements AttributeConverter<CommentStatus, String> {

		@Override
		public String convertToDatabaseColumn(CommentStatus status) {
			if (status == null) {
				return null;
			}
			return status.toString();
		}

		@Override
		public CommentStatus convertToEntityAttribute(String code) {
			if (code == null) {
				return null;
			}

			for (CommentStatus status : CommentStatus.values()) {
				if (code.equals(status.toString())) {
					return status;
				}
			}

			throw new IllegalArgumentException();
		}
	}

	private WorkItem _workItem;

	private Set<String> _toEmails = new HashSet<>();

	@NotNull
	private String _subject;

	@NotNull
	private String _body;

	@NotNull
	private CommentStatus _status;

	@NotNull
	@ElementCollection(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	public Set<String> getToEmails() {
		return this._toEmails;
	}
	
	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "workItemId", nullable = false)
	public WorkItem getWorkItem() {
		return _workItem;
	}
	
	public void preCreate() {
		if (_status == null) {
			_status = CommentStatus.Ready;
		}
	}
}
