package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;


import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class ReaderSurveyTemplateDto {
    private Long id;

    private String name;

    private String description;

    private List<ReaderSurveyTemplateQuestionDto> questions;

    private Integer xdataUserId;

    public ReaderSurveyTemplate toReaderSurveyTemplate(UserI user) {
        List<ReaderSurveyTemplateQuestion> list = new ArrayList<>();
        if (questions != null) {
            for (ReaderSurveyTemplateQuestionDto question : questions) {
                list.add(question.toReaderSurveyTemplateQuestion());
            }
        }

        ReaderSurveyTemplate readerSurveyTemplate = new ReaderSurveyTemplate();
        readerSurveyTemplate.setName(name).setDescription(description).setQuestions(list).setXdatUserId(user.getID());
        if (id != null) {
            readerSurveyTemplate.setId(id);
        }

        return readerSurveyTemplate;
    }
}
