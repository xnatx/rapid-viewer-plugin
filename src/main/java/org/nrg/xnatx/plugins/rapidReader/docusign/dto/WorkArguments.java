package org.nrg.xnatx.plugins.rapidReader.docusign.dto;

import java.io.File;

public class WorkArguments {
    private String signerEmail;
    private String signerName;
    private String ccEmail;
    private String ccName;
    private String signerEmail2;
    private String signerName2;
    private String ccEmail2;
    private String ccName2;
    private String status;

    private String fileName;
    private String docName;
    private byte[] buffer;

    private File file;
    private String transientDocumentId;

    public String getSignerEmail() {
        return signerEmail;
    }

    public void setSignerEmail(String signerEmail) {
        this.signerEmail = signerEmail;
    }

    public String getSignerName() {
        return signerName;
    }

    public void setSignerName(String signerName) {
        this.signerName = signerName;
    }

    public String getCcEmail() {
        return ccEmail;
    }

    public void setCcEmail(String ccEmail) {
        this.ccEmail = ccEmail;
    }

    public String getCcName() {
        return ccName;
    }

    public void setCcName(String ccName) {
        this.ccName = ccName;
    }

    public String getSignerEmail2() {
        return signerEmail2;
    }

    public void setSignerEmail2(String signerEmail2) {
        this.signerEmail2 = signerEmail2;
    }

    public String getSignerName2() {
        return signerName2;
    }

    public void setSignerName2(String signerName2) {
        this.signerName2 = signerName2;
    }

    public String getCcEmail2() {
        return ccEmail2;
    }

    public void setCcEmail2(String ccEmail2) {
        this.ccEmail2 = ccEmail2;
    }

    public String getCcName2() {
        return ccName2;
    }

    public void setCcName2(String ccName2) {
        this.ccName2 = ccName2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public void setBuffer(byte[] buffer) {
        this.buffer = buffer;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getTransientDocumentId() {
        return transientDocumentId;
    }

    public void setTransientDocumentId(String transientDocumentId) {
        this.transientDocumentId = transientDocumentId;
    }
}
