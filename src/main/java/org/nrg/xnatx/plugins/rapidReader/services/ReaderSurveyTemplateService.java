/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;

import java.util.Date;
import java.util.List;

public interface ReaderSurveyTemplateService extends BaseHibernateService<ReaderSurveyTemplate> {
    public ReaderSurveyTemplate createTemplate(ReaderSurveyTemplate template);
}
