/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.repositories;

import java.util.Date;
import java.util.List;

import org.apache.axis.utils.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class WorkListRepository extends AbstractHibernateDAO<WorkList> {
	@SuppressWarnings("unchecked")
	@Transactional
	public List<WorkList> getWorkLists(final Integer readerXdatUserId, String orderBy, boolean isAsc, Integer offset,
			Integer limit, final WorkListStatus status, String name, String reportId, String description,
			Date dueDateFrom, Date dueDateTo, boolean showActiveOnly) {
		final Criteria criteria = getSession().createCriteria(getParameterizedType());
		criteria.add(Restrictions.eq("readerXdatUserId", readerXdatUserId));

		if (status != null) {
			if (status != WorkListStatus.All) {
				criteria.add(Restrictions.eq("status", status));
			}
		} else if(showActiveOnly) {
			WorkListStatus[] statuses = {WorkListStatus.Open, WorkListStatus.Reviewing};
			criteria.add(Restrictions.in("status", statuses));
		}
		if (name != null && name != "") {
			criteria.add(Restrictions.ilike("name", name, MatchMode.ANYWHERE));
		}
		if (reportId != null && reportId != "") {
			criteria.add(Restrictions.ilike("reportId", reportId, MatchMode.ANYWHERE));
		}
		if (description != null && description != "") {
			criteria.add(Restrictions.ilike("description", description, MatchMode.ANYWHERE));
		}
		if (dueDateFrom != null) {
			criteria.add(Restrictions.ge("dueDate", dueDateFrom));
		}
		if (dueDateFrom != null) {
			criteria.add(Restrictions.le("dueDate", dueDateTo));
		}

		criteria.setFirstResult(offset);

		if (showActiveOnly) {
			criteria.addOrder(Order.asc("id"));
			// show all the assigned list
			// criteria.setMaxResults(1);
		} else {
			if (StringUtils.isEmpty(orderBy)) {
				criteria.addOrder(Order.asc("id"));
			} else {
				criteria.addOrder(isAsc ? Order.asc(orderBy) : Order.desc(orderBy));
			}
			criteria.setMaxResults(limit);
		}

		return criteria.list();
	}

	public WorkList findByReaderXdatUserIdAndName(Integer readerXdatUserId, String name) {
		final Criteria criteria = getSession().createCriteria(getParameterizedType());
		criteria.add(Restrictions.eq("readerXdatUserId", readerXdatUserId));
		criteria.add(Restrictions.eq("name", name));
		List<WorkList> workList =  criteria.list();

		return workList.size() == 0 ? null : workList.get(0);
	}
}
