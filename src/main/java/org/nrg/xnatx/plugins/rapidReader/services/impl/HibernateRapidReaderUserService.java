/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntityService;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidReaderUser;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.repositories.RapidReaderUserRepository;
import org.nrg.xnatx.plugins.rapidReader.services.RapidReaderUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Manages {@link WorkList} data objects in Hibernate.
 */
@Service
@Slf4j
public class HibernateRapidReaderUserService extends AbstractHibernateEntityService<RapidReaderUser, RapidReaderUserRepository>
		implements RapidReaderUserService {
	@Autowired
	private RapidReaderUserRepository _dao;

	@Transactional
	@Override
	public RapidReaderUser findByXdatUserId(Integer xdatUserId) {
		return _dao.findByXdatUserId(xdatUserId);
	}
}
