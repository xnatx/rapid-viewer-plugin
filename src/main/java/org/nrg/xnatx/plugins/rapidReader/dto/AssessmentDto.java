package org.nrg.xnatx.plugins.rapidReader.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class AssessmentDto {
	private List<AssessmentField> fields;
	
	private String reportId;
	
	private String assessorId;
	
	@NoArgsConstructor
	@AllArgsConstructor
	@Setter
	@Getter
	@ToString
	public static class AssessmentField {
		private String id;
		private String originalName;
		private String name;
		private String value;
		private String node;
	}

//	public String serializeFields() throws JsonProcessingException {
//		ObjectMapper objectMapper = new ObjectMapper();
//		return objectMapper.writeValueAsString(this.fields);
//	}
//
//	public static List<AssessmentField> deserializeFields(String json) throws JsonParseException, JsonMappingException, IOException {
//		ObjectMapper objectMapper = new ObjectMapper();
//		return objectMapper.readValue(json, new TypeReference<List<AssessmentField>>(){});
//	}
}
