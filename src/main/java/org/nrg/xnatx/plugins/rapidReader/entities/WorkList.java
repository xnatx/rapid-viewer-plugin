/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "readerXdatUserId", "name" }))
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class WorkList extends AbstractHibernateEntity {
	private static final long serialVersionUID = 1L;

	public static enum WorkListStatus {
		Open, Reviewing, Complete, Cancelled, All
	}

	@Converter(autoApply = true)
	public static class WorkListStatusConverter implements AttributeConverter<WorkListStatus, String> {

		@Override
		public String convertToDatabaseColumn(WorkListStatus status) {
			if (status == null) {
				return null;
			}
			return status.toString();
		}

		@Override
		public WorkListStatus convertToEntityAttribute(String code) {
			if (code == null) {
				return null;
			}

			for (WorkListStatus status : WorkListStatus.values()) {
				if (code.equals(status.toString())) {
					return status;
				}
			}

			throw new IllegalArgumentException();
		}
	}
	
	public static enum StorageType {
		Assessor, Database
	}
	
	@Converter(autoApply = true)
	public static class StorageTypeConverter implements AttributeConverter<StorageType, String> {

		@Override
		public String convertToDatabaseColumn(StorageType storageType) {
			if (storageType == null) {
				return null;
			}
			return storageType.toString();
		}

		@Override
		public StorageType convertToEntityAttribute(String code) {
			if (code == null) {
				return null;
			}

			for (StorageType storageType : StorageType.values()) {
				if (code.equals(storageType.toString())) {
					return storageType;
				}
			}

			throw new IllegalArgumentException();
		}
	}

	@NotNull
	private Integer _readerXdatUserId;

	private Integer _assignerXdatUserId;
	
	@NotNull
	private String _name;

	private String _description;

	@Enumerated(EnumType.STRING)
	private WorkListStatus _status;

	@NotNull
	private Date _dueDate;

	@NotNull
	private String _reportId;
	
	private String _xnatDataTypeName;	// to be used for posting xml e.g., <rad:BoneAgeAssessment...

	private Boolean _formDisabledWhenComplete = true;

	private Boolean _signRequired = false;
	
	// Comma-seperated required fields ids
	private String _requiredFieldIds;

	@Enumerated(EnumType.STRING)
	private StorageType _storageType = StorageType.Assessor;
	
	private List<WorkItem> _items;

    @JsonManagedReference
    @OneToMany(targetEntity = WorkItem.class, mappedBy = "workList", fetch = FetchType.EAGER)
    @OrderBy("id ASC")
    @Fetch(FetchMode.SELECT)
    public List<WorkItem> getItems() {
        return this._items;
    }

	private List<WorkListTime> _times;

    @JsonManagedReference
    @OneToMany(targetEntity = WorkListTime.class, mappedBy = "workList", fetch = FetchType.EAGER)
    @OrderBy("id ASC")
    @Fetch(FetchMode.SELECT)
    public List<WorkListTime> getTimes() {
        return this._times;
    }

    private Long _surveyTemplateId;

	private Date _finishedDate;

	private String _envelopeId;

	private Boolean _evaluating = false;

	private String _signatureServiceProvider;

//	private ReaderSurvey _openingSurvey;
//
//	@OneToOne
//	@JoinColumn(name = "openingSurveyId", nullable = true)
//	public ReaderSurvey getOpeningSurvey() {
//		return _openingSurvey;
//	}
//
//	private ReaderSurvey _closingSurvey;
//
//	@OneToOne
//	@JoinColumn(name = "closingSurveyId", nullable = true)
//	public ReaderSurvey getClosingSurvey() {
//		return _closingSurvey;
//	}

	public void preCreate() {
		if (_status == null) {
			_status = WorkListStatus.Open;
		}
	}
}
