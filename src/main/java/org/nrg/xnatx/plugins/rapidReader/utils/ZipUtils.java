package org.nrg.xnatx.plugins.rapidReader.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class ZipUtils {
    private ZipUtils() {}

    public static byte[] zipWithChecksum(byte[] reportFileBytes, String fileNameWithoutExtension) throws IOException, NoSuchAlgorithmException {
        String checksum = generateSha256Checksum(reportFileBytes);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (ZipOutputStream zos = new ZipOutputStream(baos)) {
            ZipEntry reportFileEntry = new ZipEntry(fileNameWithoutExtension + ".csv");
            zos.putNextEntry(reportFileEntry);
            zos.write(reportFileBytes);
            zos.closeEntry();

            ZipEntry checksumFileEntry = new ZipEntry(fileNameWithoutExtension + ".sha256");
            zos.putNextEntry(checksumFileEntry);
            zos.write(checksum.getBytes());
            zos.closeEntry();
        }
        return baos.toByteArray();
    }

    public static String generateSha256Checksum(byte[] inputs) throws IOException, NoSuchAlgorithmException {
        MessageDigest shaDigest = MessageDigest.getInstance("SHA-256");
        return generateChecksum(shaDigest, inputs);
    }

    public static String generateChecksum(MessageDigest digest, byte[] inputs) throws IOException {
        byte[] outputs = digest.digest(inputs);

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < outputs.length; i++) {
            sb.append(Integer.toString((outputs[i] & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }
}
