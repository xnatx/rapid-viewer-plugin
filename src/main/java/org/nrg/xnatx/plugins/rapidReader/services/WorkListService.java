/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import java.util.Date;
import java.util.List;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;

public interface WorkListService extends BaseHibernateService<WorkList> {
	WorkList findById(final Long id);

	List<WorkList> getWorkLists(final Integer readerXdatUserId, String orderBy, boolean isAsc, Integer offset,
			Integer limit, final WorkListStatus status, String name, String reportId, String description, Date dueDateFrom, Date dueDateTo, boolean showActiveOnly);

	List<WorkList> findByProjectId(String projectId);

	WorkList findByReaderXdatUserIdAndName(Integer readerXdatUserId, String name);
}
