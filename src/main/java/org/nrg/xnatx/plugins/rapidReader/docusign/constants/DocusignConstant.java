package org.nrg.xnatx.plugins.rapidReader.docusign.constants;

public class DocusignConstant {
    public static final String EMAIL_SUBJECT = "[Rapid Reader] Please sign the summary report";
    public static final String EMAIL_BODY = "Please review and sign the Rapid Reader summary report.";
    public static final String DOCUMENT_NAME = "Rapid Reader Summary Report";
}
