/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.repositories;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xnatx.plugins.rapidReader.entities.RapidReaderUser;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class RapidReaderUserRepository extends AbstractHibernateDAO<RapidReaderUser> {

    @SuppressWarnings("unchecked")
    @Transactional
    public RapidReaderUser findByXdatUserId(Integer xdatUserId) {
        final Criteria criteria = getSession().createCriteria(getParameterizedType());
        criteria.add(Restrictions.eq("xdatUserId", xdatUserId));

        List<RapidReaderUser> rapidReaderUsers = criteria.list();
        return rapidReaderUsers.size() == 0 ? null : rapidReaderUsers.get(0);
    }
}
