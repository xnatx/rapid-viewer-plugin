package org.nrg.xnatx.plugins.rapidReader.etc;

public enum RapidReaderUserType {
	SiteAdmin, Reader, Assigner
}
