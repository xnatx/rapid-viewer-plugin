package org.nrg.xnatx.plugins.rapidReader.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xft.security.UserI;
import org.nrg.xnatx.plugins.rapidReader.dto.ReaderSurveyTemplateDto;
import org.nrg.xnatx.plugins.rapidReader.dto.ReaderSurveyTemplateQuestionDto;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;
import org.nrg.xnatx.plugins.rapidReader.services.*;
import org.nrg.xnatx.plugins.rapidReader.services.impl.PermissionService;
import org.nrg.xnatx.plugins.rapidReader.utils.PluginException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

import static org.nrg.xdat.security.helpers.AccessLevel.Admin;

@Api
@XapiRestController
@RequestMapping(value = "/surveyTemplates")
@Slf4j
public class RapidSurveyTemplateApi extends AbstractXapiRestController {
    @Autowired
    protected RapidSurveyTemplateApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
                                     ReaderSurveyTemplateService readerSurveyTemplateService, ReaderSurveyTemplateQuestionService readerSurveyTemplateQuestionService, final PermissionService permissionService) {
        super(userManagementService, roleHolder);
        _userManagementService = userManagementService;
        _readerSurveyTemplateService = readerSurveyTemplateService;
        _readerSurveyTemplateQuestionService = readerSurveyTemplateQuestionService;
        _permissionService = permissionService;
    }

    @ApiOperation(value = "Add a survey template.", response = ReaderSurveyTemplate.class)
    @ApiResponses({@ApiResponse(code = 200, message = "survey template successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST, restrictTo = Admin)
    public ReaderSurveyTemplate addSurveyTemplate(Principal principal,
                                                  @RequestBody ReaderSurveyTemplateDto dto) throws PluginException, NotFoundException, JsonProcessingException {
        log.trace("/surveyTemplates [POST] called: {}}", dto.toString());

        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        final ReaderSurveyTemplate template = dto.toReaderSurveyTemplate(user);

        _readerSurveyTemplateService.createTemplate(template);

        return template;
    }

    @ApiOperation(value = "Returns a list of all survey templates.", response = ReaderSurveyTemplate.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "survey templates successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET, restrictTo = Admin)
    public List<ReaderSurveyTemplate> getAll(Principal principal)
            throws NotFoundException {
        log.trace("/surveyTemplates [GET] called");
        return _readerSurveyTemplateService.getAll();
    }

    private final UserManagementServiceI _userManagementService;
    private final ReaderSurveyTemplateService _readerSurveyTemplateService;
    private final ReaderSurveyTemplateQuestionService _readerSurveyTemplateQuestionService;
    private final PermissionService _permissionService;

    @ApiOperation(value = "Add a survey template.", response = ReaderSurveyTemplate.class)
    @ApiResponses({@ApiResponse(code = 200, message = "survey template successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{templateId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT, restrictTo = Admin)
    public ReaderSurveyTemplate updateSurveyTemplate(
            Principal principal,
            @PathVariable final Long templateId,
            @RequestBody ReaderSurveyTemplateDto dto
    ) throws PluginException, NotFoundException, JsonProcessingException {
        log.trace("/surveyTemplates/{} [PUT] called: {}}", templateId, dto.toString());

        try {
            ReaderSurveyTemplate existing = _readerSurveyTemplateService.get(templateId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("Template not found");
        }

        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        final ReaderSurveyTemplate template = dto.toReaderSurveyTemplate(user);
        template.setId(templateId);

        _readerSurveyTemplateService.update(template);

        return template;
    }

    @ApiOperation(value = "Add a survey template question.", response = ReaderSurveyTemplate.class)
    @ApiResponses({@ApiResponse(code = 200, message = "survey template successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{templateId}/questions", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.POST, restrictTo = Admin)
    public ReaderSurveyTemplateQuestion addSurveyTemplateQuestion(
            Principal principal,
            @PathVariable final Long templateId,
            @RequestBody ReaderSurveyTemplateQuestionDto dto) throws PluginException, NotFoundException, JsonProcessingException {
        log.trace("/surveyTemplates/{}/questions [POST] called: {}}", templateId, dto.toString());

        ReaderSurveyTemplate existing;
        try {
            existing = _readerSurveyTemplateService.get(templateId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("Template not found");
        }

        UserI user = XnatUtil.readUser(_userManagementService, principal.getName());
        final ReaderSurveyTemplateQuestion question = dto.toReaderSurveyTemplateQuestion();
        question.setReaderSurveyTemplate(existing);

        return _readerSurveyTemplateQuestionService.create(question);
    }


    @ApiOperation(value = "Update a survey template question.", response = ReaderSurveyTemplateQuestion.class)
    @ApiResponses({@ApiResponse(code = 200, message = "survey template successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{templateId}/questions/{questionId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.PUT, restrictTo = Admin)
    public ReaderSurveyTemplateQuestion updateSurveyTemplateQuestion(
            Principal principal,
            @PathVariable final Long templateId,
            @PathVariable final Long questionId,
            @RequestBody ReaderSurveyTemplateQuestionDto dto) throws PluginException, NotFoundException, JsonProcessingException {
        log.trace("/templates/{}/questions/{} [PUT] called: {}}", templateId, questionId, dto.toString());

        ReaderSurveyTemplate existing;
        try {
            existing = _readerSurveyTemplateService.get(templateId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("Template not found");
        }

        ReaderSurveyTemplateQuestion question;
        try {
            question = _readerSurveyTemplateQuestionService.get(questionId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("Question not found");
        }
//        final ReaderSurveyTemplateQuestion question = dto.toReaderSurveyTemplateQuestion();
        question.setQuestion(dto.getQuestion());
        question.setType(dto.getType());
        question.setReaderSurveyTemplate(existing);

        _readerSurveyTemplateQuestionService.update(question);
        return question;
    }

    @ApiOperation(value = "Update a survey template question.", response = ReaderSurveyTemplateQuestion.class)
    @ApiResponses({@ApiResponse(code = 200, message = "survey template successfully created."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{templateId}/questions/{questionId}", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.DELETE, restrictTo = Admin)
    public ReaderSurveyTemplateQuestion deleteSurveyTemplateQuestion(
            Principal principal,
            @PathVariable final Long templateId,
            @PathVariable final Long questionId
    ) throws PluginException, NotFoundException, JsonProcessingException {
        log.trace("/surveyTemplates/{}/questions/{} [DELETE] called}", templateId, questionId);

        ReaderSurveyTemplate existing;
        try {
            existing = _readerSurveyTemplateService.get(templateId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("Template not found");
        }

        ReaderSurveyTemplateQuestion question;
        try {
            question = _readerSurveyTemplateQuestionService.get(questionId);
        } catch (org.nrg.framework.exceptions.NotFoundException e) {
            throw new NotFoundException("Question not found");
        }

        question.setEnabled(false);

        _readerSurveyTemplateQuestionService.update(question);
        return question;
    }

    @ApiOperation(value = "Returns a list of all survey template questions.", response = ReaderSurveyTemplate.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "survey templates successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "{templateId}/questions", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET)
    public List<ReaderSurveyTemplateQuestion> getQuestions(
            Principal principal,
            @PathVariable final Long templateId
    ) throws NotFoundException {
        log.trace("/surveyTemplates/{}/questions [GET] called", templateId);
        return _readerSurveyTemplateQuestionService.getQuestions(templateId);
    }
}
