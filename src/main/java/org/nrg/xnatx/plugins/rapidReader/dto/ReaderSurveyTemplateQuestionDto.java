package org.nrg.xnatx.plugins.rapidReader.dto;

import lombok.*;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplate;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion;
import org.nrg.xnatx.plugins.rapidReader.entities.ReaderSurveyTemplateQuestion.QuestionType;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class ReaderSurveyTemplateQuestionDto {
    private String question;

    private QuestionType type;

    public ReaderSurveyTemplateQuestion toReaderSurveyTemplateQuestion() {
        ReaderSurveyTemplateQuestion readerSurveyTemplateQuestion = new ReaderSurveyTemplateQuestion();
        return readerSurveyTemplateQuestion.setQuestion(question).setType(type);
    }
}
