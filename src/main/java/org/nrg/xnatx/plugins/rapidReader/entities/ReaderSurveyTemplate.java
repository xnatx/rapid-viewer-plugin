package org.nrg.xnatx.plugins.rapidReader.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.nrg.framework.orm.hibernate.AbstractHibernateEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Accessors(prefix = "_", chain = true)
@Cache(usage = CacheConcurrencyStrategy.NONE, region = "nrg")
@ToString
public class ReaderSurveyTemplate extends AbstractHibernateEntity {
    private static final long serialVersionUID = 1L;

    @NotNull
    private String _name;

    private String _description;

    private List<ReaderSurveyTemplateQuestion> _questions = new ArrayList<>();

    @NotNull
    private Integer _xdatUserId;

    @JsonManagedReference
    @OneToMany(targetEntity = ReaderSurveyTemplateQuestion.class, mappedBy = "readerSurveyTemplate", fetch = FetchType.EAGER)
    @OrderBy("id ASC")
    @Fetch(FetchMode.SELECT)
    public List<ReaderSurveyTemplateQuestion> getQuestions() {
        return this._questions;
    }

    @Column(columnDefinition="TEXT")
    public String getDescription() { return _description; }
}
