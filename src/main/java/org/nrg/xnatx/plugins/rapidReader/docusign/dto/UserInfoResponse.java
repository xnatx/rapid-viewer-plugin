package org.nrg.xnatx.plugins.rapidReader.docusign.dto;

import com.google.api.client.util.Key;

import java.util.List;

public class UserInfoResponse {
    public static class Account {
        @Key("account_id")
        private String accountId;

        @Key("is_default")
        private Boolean isDefault;

        @Key("account_name")
        private String accountName;

        @Key("base_uri")
        private String baseUri;

        public String getAccountId() {
            return accountId;
        }

        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        public Boolean getDefault() {
            return isDefault;
        }

        public void setDefault(Boolean aDefault) {
            isDefault = aDefault;
        }

        public String getAccountName() {
            return accountName;
        }

        public void setAccountName(String accountName) {
            this.accountName = accountName;
        }

        public String getBaseUri() {
            return baseUri;
        }

        public void setBaseUri(String baseUri) {
            this.baseUri = baseUri;
        }

        @Override
        public String toString() {
            return "Account{" +
                    "accountId='" + accountId + '\'' +
                    ", isDefault=" + isDefault +
                    ", accountName='" + accountName + '\'' +
                    ", baseUri='" + baseUri + '\'' +
                    '}';
        }
    }

    @Key("sub")
    private String apiUsername;

    @Key
    private String name;

    @Key("given_name")
    private String givenName;

    @Key("family_name")
    private String familyName;

    @Key("created")
    private String created;

    @Key("email")
    private String email;

    @Key("accounts")
    private List<Account> accounts;

    public String getApiUsername() {
        return apiUsername;
    }

    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "UserInfoResponse{" +
                "apiUsername='" + apiUsername + '\'' +
                ", name='" + name + '\'' +
                ", givenName='" + givenName + '\'' +
                ", familyName='" + familyName + '\'' +
                ", created=" + created +
                ", email='" + email + '\'' +
                ", accounts=" + accounts +
                '}';
    }
}
