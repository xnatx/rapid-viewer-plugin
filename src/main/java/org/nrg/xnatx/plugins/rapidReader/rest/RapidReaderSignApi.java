/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.rest;

import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.nrg.framework.annotations.XapiRestController;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xapi.rest.AbstractXapiRestController;
import org.nrg.xapi.rest.XapiRequestMapping;
import org.nrg.xdat.security.services.RoleHolder;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.SignaturePreferences;
import org.nrg.xnatx.plugins.rapidReader.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

import static org.nrg.xdat.security.helpers.AccessLevel.Null;

@Api
@XapiRestController
@RequestMapping(value = "/rapidReader/sign")
@Slf4j
public class RapidReaderSignApi extends AbstractXapiRestController {
    @Autowired
    protected RapidReaderSignApi(final UserManagementServiceI userManagementService, final RoleHolder roleHolder,
                                 SignaturePreferences docusignPrefs) {
        super(userManagementService, roleHolder);
        _docusignPrefs = docusignPrefs;
    }

    @ApiOperation(value = "Refreshes Adobe access token", response = UserDto.class, responseContainer = "List")
    @ApiResponses({@ApiResponse(code = 200, message = "Users successfully retrieved."),
            @ApiResponse(code = 401, message = "Must be authenticated to access the XNAT REST API."),
            @ApiResponse(code = 500, message = "Unexpected error")})
    @XapiRequestMapping(value = "accessToken", produces = {
            MediaType.APPLICATION_JSON_VALUE}, method = RequestMethod.GET, restrictTo = Null)
    public void refreshAccessToken(
            final HttpServletRequest request,
            @ApiParam(value = "code", required = true) @RequestParam(required = true) final String code,
            @ApiParam(value = "api_access_point", required = true) @RequestParam(required = true) final String apiAccessPoint,
            @ApiParam(value = "web_access_point", required = true) @RequestParam(required = true) final String webAccessPoint
            )
            throws NotFoundException {
        log.trace("/rapidReader/sign/accessToken");

        String referrer = request.getHeader("referer");
        log.trace("referrer: {}", referrer);

        _docusignPrefs.setCode(code);
        _docusignPrefs.setApiAccessPoint(apiAccessPoint);
        _docusignPrefs.setWebAccessPoint(webAccessPoint);
    }

    private final SignaturePreferences _docusignPrefs;
}
