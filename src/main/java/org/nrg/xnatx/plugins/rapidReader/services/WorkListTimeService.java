/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.services;

import org.nrg.framework.orm.hibernate.BaseHibernateService;
import org.nrg.xapi.exceptions.NotFoundException;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkListTime;

import java.util.List;

public interface WorkListTimeService extends BaseHibernateService<WorkListTime> {
	List<WorkListTime> getWorkListTimes(final Long workListId);

	WorkListTime createWorkListTime(final Long workListId) throws NotFoundException;

	WorkListTime updateWorkListTime(final Long workListTimeId, final Boolean abnormallyStopped, final String stopReason) throws NotFoundException;
}
