/*
 * XNAT http://www.xnat.org
 * Copyright (c) 2020, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xnatx.plugins.rapidReader.dto;

import org.nrg.xft.security.UserI;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Setter
@Getter
@ToString
public class UserDto {

	private Integer xdatUserId;

	private String username;

	private String email;
	
	private String firstName;
	
	private String lastName;
	
	public static UserDto fromUser(UserI user) {
		return UserDto.builder().xdatUserId(user.getID()).username(user.getUsername()).email(user.getEmail())
		.firstName(user.getFirstname()).lastName(user.getLastname()).build();
	}
}
