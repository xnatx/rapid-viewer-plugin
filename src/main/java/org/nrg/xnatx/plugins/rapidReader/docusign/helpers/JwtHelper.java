package org.nrg.xnatx.plugins.rapidReader.docusign.helpers;

import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.nrg.xnatx.plugins.rapidReader.utils.DateUtils;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.security.Key;
import java.security.KeyFactory;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;

@Slf4j
public class JwtHelper {
    public static Key loadKey(String privateKey) throws Exception {
        return JwtHelper.readPrivateKey(privateKey);
    }

    public static String constructJws(
            Key key,
            String issuer,
            String subject,
            String audience,
            String scope,
            Date issuedAt,
            Date expirationTime) {
        log.debug("Using Builder to construct Jws");
        return Jwts.builder()
                .setIssuer(issuer)
                .setSubject(subject)
                .setAudience(audience)
                .setIssuedAt(issuedAt)
                .setExpiration(expirationTime)
                .claim("scope", scope)
                .signWith(key)
                .compact();
    }

    public static String constructJws(
            Key key,
            String issuer,
            String subject,
            String audience,
            String scope,
            Date issuedAt,
            int expiredAfterSecs) {
        return JwtHelper.constructJws(key, issuer, subject, audience, scope, issuedAt, DateUtils.addSeconds(issuedAt, expiredAfterSecs));
    }

    public static RSAPrivateKey readPrivateKey(String privateKey) throws Exception {
        StringReader keyReader = new StringReader(privateKey);
        return JwtHelper.readPrivateKey(keyReader);
    }

    public static RSAPrivateKey readPrivateKey(File file) throws Exception {
        try (FileReader keyReader = new FileReader(file)) {
            return JwtHelper.readPrivateKey(keyReader);
        }
    }

    private static RSAPrivateKey readPrivateKey(Reader keyReader) throws Exception {
        log.debug("Reading private key");
        try (PemReader pemReader = new PemReader(keyReader)) {
            KeyFactory factory = KeyFactory.getInstance("RSA");

            PemObject pemObject = pemReader.readPemObject();
            byte[] content = pemObject.getContent();
            PKCS8EncodedKeySpec privyKeySpec = new PKCS8EncodedKeySpec(content);
            return (RSAPrivateKey) factory.generatePrivate(privyKeySpec);
        }
    }
}
