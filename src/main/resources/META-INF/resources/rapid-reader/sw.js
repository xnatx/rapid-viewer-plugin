/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/rapid-reader/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

// https://developers.google.com/web/tools/workbox/guides/troubleshoot-and-debug
importScripts('https://storage.googleapis.com/workbox-cdn/releases/5.0.0-beta.1/workbox-sw.js'); // Install newest
// https://developers.google.com/web/tools/workbox/modules/workbox-core

workbox.core.skipWaiting();
workbox.core.clientsClaim(); // Cache static assets that aren't precached

workbox.routing.registerRoute(/\.(?:js|css)$/, new workbox.strategies.StaleWhileRevalidate({
  cacheName: 'static-resources'
})); // Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.

workbox.routing.registerRoute(/^https:\/\/fonts\.googleapis\.com/, new workbox.strategies.StaleWhileRevalidate({
  cacheName: 'google-fonts-stylesheets'
})); // Cache the underlying font files with a cache-first strategy for 1 year.

workbox.routing.registerRoute(/^https:\/\/fonts\.gstatic\.com/, new workbox.strategies.CacheFirst({
  cacheName: 'google-fonts-webfonts',
  plugins: [new workbox.cacheableResponse.CacheableResponsePlugin({
    statuses: [0, 200]
  }), new workbox.expiration.ExpirationPlugin({
    maxAgeSeconds: 60 * 60 * 24 * 365,
    // 1 Year
    maxEntries: 30
  })]
})); // MESSAGE HANDLER

self.addEventListener('message', function (event) {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    switch (event.data.type) {
      case 'SKIP_WAITING':
        // TODO: We'll eventually want this to be user prompted
        // workbox.core.skipWaiting();
        // workbox.core.clientsClaim();
        // TODO: Global notification to indicate incoming reload
        break;

      default:
        console.warn("SW: Invalid message type: ".concat(event.data.type));
    }
  }
});
workbox.precaching.precacheAndRoute([{"revision":"90ccb147f0211a55fbf2f8ca81525591","url":"/rapid-reader/0.24b1b13ff8238a4a42d3.css"},{"revision":"0369069720a6de49f7d59666dc267ad2","url":"/rapid-reader/10.24b1b13ff8238a4a42d3.css"},{"revision":"a81a1615b15a9fc3f57cb7b7c57f20e6","url":"/rapid-reader/15.bundle.003f050af3559a92cbfe.js"},{"revision":"37c6b1e97da96446a8c1dfb903184661","url":"/rapid-reader/16.24b1b13ff8238a4a42d3.css"},{"revision":"8266a4e5bf94c9f4df836e6e8409fa2d","url":"/rapid-reader/16.bundle.474d58aec886abaefc75.js"},{"revision":"c0e2e5bafed1fd58937c3c2b0baddb5c","url":"/rapid-reader/17.bundle.f3d36e8fc3962ba632a0.js"},{"revision":"0cf1e49ce4bd1b671d151d837f7c3ec3","url":"/rapid-reader/18.24b1b13ff8238a4a42d3.css"},{"revision":"732daebd41d111549eceb6559d775408","url":"/rapid-reader/18.bundle.32f076f3042092b6992c.js"},{"revision":"2625d1e558407a60e7b9a096615ba423","url":"/rapid-reader/19.24b1b13ff8238a4a42d3.css"},{"revision":"0b02920ea2b457b354b74ed9921acd06","url":"/rapid-reader/19.bundle.65e4538f92815a97a7b1.js"},{"revision":"648405482833e711f9d56f3a9c66a69c","url":"/rapid-reader/2.24b1b13ff8238a4a42d3.css"},{"revision":"1eb3cdc0c1d01eb1e6ea84e72e9647f0","url":"/rapid-reader/20.bundle.f25175e771df10a0c7f0.js"},{"revision":"91feab40ec70804e03cb195c6c626947","url":"/rapid-reader/21.bundle.754c1cb404ae52811c5f.js"},{"revision":"63b7a813c0298d6afa986c8d3f1490c2","url":"/rapid-reader/22.bundle.11d0f316411cb1757ad2.js"},{"revision":"b7f24bb1aed70ca1e766ad44e93e1b48","url":"/rapid-reader/4.bundle.9e3c1f835db214304de8.js"},{"revision":"0731d3258f53b657979535b290f4c7a3","url":"/rapid-reader/8.24b1b13ff8238a4a42d3.css"},{"revision":"260f7b0ecd47a2d3473d498bf9653b59","url":"/rapid-reader/9.24b1b13ff8238a4a42d3.css"},{"revision":"fceecbf559225cd71b63892a7402b179","url":"/rapid-reader/CallbackPage.bundle.e27a95c0965089695caa.js"},{"revision":"247a3ef461e16a51fb2e3abf46d4f7a1","url":"/rapid-reader/ConnectedStandaloneRouting.bundle.68cc9fe8108e9f50e55e.js"},{"revision":"fe2c80034318226dc00d0a561105f511","url":"/rapid-reader/ConnectedStandaloneRouting~ConnectedXNATStandaloneRouting~IHEInvokeImageDisplay~StudyListRouting~Vie~432150a4.bundle.5999efb388c1b465d949.js"},{"revision":"4d73c5dd937225d0c22fe1fa374d4486","url":"/rapid-reader/ConnectedStandaloneRouting~ConnectedXNATStandaloneRouting~IHEInvokeImageDisplay~ViewerLocalFileData~~e8534976.bundle.b0297d028de52f9665f9.js"},{"revision":"2d9761c91636804a4c335e48b34487bd","url":"/rapid-reader/ConnectedXNATStandaloneRouting.bundle.2801701f0a3dc04c34d7.js"},{"revision":"77abda450c201f2d161ce22b58a4377f","url":"/rapid-reader/IHEInvokeImageDisplay.bundle.8f3a4074423d666b1479.js"},{"revision":"06c18da62018b617cf3477853221c62f","url":"/rapid-reader/StudyListRouting.bundle.e77dab0a4580729bf800.js"},{"revision":"b95e6094c9ef03a4e5edf6213f5da560","url":"/rapid-reader/ViewerLocalFileData.bundle.07d0a6decfd45e49278b.js"},{"revision":"baadf75f9beb4282de3049b2d256f332","url":"/rapid-reader/ViewerRouting.bundle.1484900982c28e9e27b6.js"},{"revision":"64c92f571fe4989bc04d2432b9c1f493","url":"/rapid-reader/WorkListRouting.bundle.cc21f9f9ed1fd9b761c1.js"},{"revision":"04b9e9d06d51571b9f101241ffb93a33","url":"/rapid-reader/app-config.js"},{"revision":"cdf3798fd6a730662987c48aa8fa44fa","url":"/rapid-reader/app.24b1b13ff8238a4a42d3.css"},{"revision":"473e74a795f5a95dcfba304960bbcdf8","url":"/rapid-reader/assets/Button_File.svg"},{"revision":"271da60b435c1445580caab72e656818","url":"/rapid-reader/assets/Button_Folder.svg"},{"revision":"cb4f64534cdf8dd88f1d7219d44490db","url":"/rapid-reader/assets/android-chrome-144x144.png"},{"revision":"5cde390de8a619ebe55a669d2ac3effd","url":"/rapid-reader/assets/android-chrome-192x192.png"},{"revision":"e7466a67e90471de05401e53b8fe20be","url":"/rapid-reader/assets/android-chrome-256x256.png"},{"revision":"9bbe9b80156e930d19a4e1725aa9ddae","url":"/rapid-reader/assets/android-chrome-36x36.png"},{"revision":"5698b2ac0c82fe06d84521fc5482df04","url":"/rapid-reader/assets/android-chrome-384x384.png"},{"revision":"56bef3fceec344d9747f8abe9c0bba27","url":"/rapid-reader/assets/android-chrome-48x48.png"},{"revision":"3e8b8a01290992e82c242557417b0596","url":"/rapid-reader/assets/android-chrome-512x512.png"},{"revision":"517925e91e2ce724432d296b687d25e2","url":"/rapid-reader/assets/android-chrome-72x72.png"},{"revision":"4c3289bc690f8519012686888e08da71","url":"/rapid-reader/assets/android-chrome-96x96.png"},{"revision":"cf464289183184df09292f581df0fb4f","url":"/rapid-reader/assets/apple-touch-icon-1024x1024.png"},{"revision":"0857c5282c594e4900e8b31e3bade912","url":"/rapid-reader/assets/apple-touch-icon-114x114.png"},{"revision":"4208f41a28130a67e9392a9dfcee6011","url":"/rapid-reader/assets/apple-touch-icon-120x120.png"},{"revision":"cb4f64534cdf8dd88f1d7219d44490db","url":"/rapid-reader/assets/apple-touch-icon-144x144.png"},{"revision":"977d293982af7e9064ba20806b45cf35","url":"/rapid-reader/assets/apple-touch-icon-152x152.png"},{"revision":"6de91b4d2a30600b410758405cb567b4","url":"/rapid-reader/assets/apple-touch-icon-167x167.png"},{"revision":"87bff140e3773bd7479a620501c4aa5c","url":"/rapid-reader/assets/apple-touch-icon-180x180.png"},{"revision":"647386c34e75f1213830ea9a38913525","url":"/rapid-reader/assets/apple-touch-icon-57x57.png"},{"revision":"0c200fe83953738b330ea431083e7a86","url":"/rapid-reader/assets/apple-touch-icon-60x60.png"},{"revision":"517925e91e2ce724432d296b687d25e2","url":"/rapid-reader/assets/apple-touch-icon-72x72.png"},{"revision":"c9989a807bb18633f6dcf254b5b56124","url":"/rapid-reader/assets/apple-touch-icon-76x76.png"},{"revision":"87bff140e3773bd7479a620501c4aa5c","url":"/rapid-reader/assets/apple-touch-icon-precomposed.png"},{"revision":"87bff140e3773bd7479a620501c4aa5c","url":"/rapid-reader/assets/apple-touch-icon.png"},{"revision":"05fa74ea9c1c0c3931ba96467999081d","url":"/rapid-reader/assets/apple-touch-startup-image-1182x2208.png"},{"revision":"9e2cd03e1e6fd0520eea6846f4278018","url":"/rapid-reader/assets/apple-touch-startup-image-1242x2148.png"},{"revision":"5591e3a1822cbc8439b99c1a40d53425","url":"/rapid-reader/assets/apple-touch-startup-image-1496x2048.png"},{"revision":"337de578c5ca04bd7d2be19d24d83821","url":"/rapid-reader/assets/apple-touch-startup-image-1536x2008.png"},{"revision":"cafb4ab4eafe6ef946bd229a1d88e7de","url":"/rapid-reader/assets/apple-touch-startup-image-320x460.png"},{"revision":"d9bb9e558d729eeac5efb8be8d6111cc","url":"/rapid-reader/assets/apple-touch-startup-image-640x1096.png"},{"revision":"038b5b02bac8b82444bf9a87602ac216","url":"/rapid-reader/assets/apple-touch-startup-image-640x920.png"},{"revision":"2177076eb07b1d64d663d7c03268be00","url":"/rapid-reader/assets/apple-touch-startup-image-748x1024.png"},{"revision":"4fc097443815fe92503584c4bd73c630","url":"/rapid-reader/assets/apple-touch-startup-image-750x1294.png"},{"revision":"2e29914062dce5c5141ab47eea2fc5d9","url":"/rapid-reader/assets/apple-touch-startup-image-768x1004.png"},{"revision":"f692ec286b3a332c17985f4ed38b1076","url":"/rapid-reader/assets/browserconfig.xml"},{"revision":"f3d9a3b647853c45b0e132e4acd0cc4a","url":"/rapid-reader/assets/coast-228x228.png"},{"revision":"533ba1dcac7b716dec835a2fae902860","url":"/rapid-reader/assets/favicon-16x16.png"},{"revision":"783e9edbcc23b8d626357ca7101161e0","url":"/rapid-reader/assets/favicon-32x32.png"},{"revision":"0711f8e60267a1dfc3aaf1e3818e7185","url":"/rapid-reader/assets/favicon.ico"},{"revision":"5df2a5b0cee399ac0bc40af74ba3c2cb","url":"/rapid-reader/assets/firefox_app_128x128.png"},{"revision":"11fd9098c4b07c8a07e1d2a1e309e046","url":"/rapid-reader/assets/firefox_app_512x512.png"},{"revision":"27cddfc922dca3bfa27b4a00fc2f5e36","url":"/rapid-reader/assets/firefox_app_60x60.png"},{"revision":"2017d95fae79dcf34b5a5b52586d4763","url":"/rapid-reader/assets/manifest.webapp"},{"revision":"cb4f64534cdf8dd88f1d7219d44490db","url":"/rapid-reader/assets/mstile-144x144.png"},{"revision":"334895225e16a7777e45d81964725a97","url":"/rapid-reader/assets/mstile-150x150.png"},{"revision":"e295cca4af6ed0365cf7b014d91b0e9d","url":"/rapid-reader/assets/mstile-310x150.png"},{"revision":"cbefa8c42250e5f2443819fe2c69d91e","url":"/rapid-reader/assets/mstile-310x310.png"},{"revision":"aa411a69df2b33a1362fa38d1257fa9d","url":"/rapid-reader/assets/mstile-70x70.png"},{"revision":"5609af4f69e40e33471aee770ea1d802","url":"/rapid-reader/assets/yandex-browser-50x50.png"},{"revision":"cfea70d7ddc8f06f276ea0c85c4b2adf","url":"/rapid-reader/assets/yandex-browser-manifest.json"},{"revision":"0ca44a1b8719e835645ffa804a9d1395","url":"/rapid-reader/es6-shim.min.js"},{"revision":"020b236e8206f4ca35f3143907de817e","url":"/rapid-reader/google.js"},{"revision":"e78cc04bcda274b3b63ec2f5d31eb947","url":"/rapid-reader/index.html"},{"revision":"551db51d657075bba9f53b9027b2c526","url":"/rapid-reader/init-service-worker.js"},{"revision":"69a775a606c7a7892c32d9df0d0bf105","url":"/rapid-reader/itk/ImageIOs/itkBMPImageIOJSBinding.js"},{"revision":"49b2c7547ece59c18eb7665c3ccc0f8b","url":"/rapid-reader/itk/ImageIOs/itkBMPImageIOJSBindingWasm.js"},{"revision":"95ff869ad483e082b9c99f073607a86d","url":"/rapid-reader/itk/ImageIOs/itkBMPImageIOJSBindingWasm.wasm"},{"revision":"0686ce06fd889529d91318f60b12025d","url":"/rapid-reader/itk/ImageIOs/itkBioRadImageIOJSBinding.js"},{"revision":"41ff0c76790159239b8322df57a8f916","url":"/rapid-reader/itk/ImageIOs/itkBioRadImageIOJSBindingWasm.js"},{"revision":"d815a2fc778b7f60ec8436e7b651349c","url":"/rapid-reader/itk/ImageIOs/itkBioRadImageIOJSBindingWasm.wasm"},{"revision":"9c5dee14ad3214cdc6190cbe14f5d6d5","url":"/rapid-reader/itk/ImageIOs/itkDCMTKImageIOJSBindingWasm.js"},{"revision":"d6f994ac9b851374c95c3feece91f847","url":"/rapid-reader/itk/ImageIOs/itkDICOMImageSeriesReaderJSBindingWasm.js"},{"revision":"58b0a13aa5b0026a94529f8e87a9cec1","url":"/rapid-reader/itk/ImageIOs/itkFDFImageIOJSBinding.js"},{"revision":"094bff63a913f23aa23285a6c1fba595","url":"/rapid-reader/itk/ImageIOs/itkFDFImageIOJSBindingWasm.js"},{"revision":"891d35cad01f58ecf51a24ab2d907632","url":"/rapid-reader/itk/ImageIOs/itkFDFImageIOJSBindingWasm.wasm"},{"revision":"0d4002b76b33a48349e821487e6027e0","url":"/rapid-reader/itk/ImageIOs/itkGDCMImageIOJSBindingWasm.js"},{"revision":"b6401e6e4fb2c356e6a26a68bbc49672","url":"/rapid-reader/itk/ImageIOs/itkGE4ImageIOJSBinding.js"},{"revision":"9a809941e109af19dfe88ee6318b080f","url":"/rapid-reader/itk/ImageIOs/itkGE4ImageIOJSBindingWasm.js"},{"revision":"dc64ef4ac30d9ad34689a45868c99a47","url":"/rapid-reader/itk/ImageIOs/itkGE4ImageIOJSBindingWasm.wasm"},{"revision":"af62437786a81fc6993b09910b35a656","url":"/rapid-reader/itk/ImageIOs/itkGE5ImageIOJSBinding.js"},{"revision":"7f8cff76fe893116e337af14d5fa3a3c","url":"/rapid-reader/itk/ImageIOs/itkGE5ImageIOJSBindingWasm.js"},{"revision":"82c094aa8a3d1c1c4617be1fa3d0ea0c","url":"/rapid-reader/itk/ImageIOs/itkGE5ImageIOJSBindingWasm.wasm"},{"revision":"2fc2e29aacdd8bf5a74476dff48963cb","url":"/rapid-reader/itk/ImageIOs/itkGEAdwImageIOJSBinding.js"},{"revision":"9c81f066a2dd3b5da9e82e2ca120a9fc","url":"/rapid-reader/itk/ImageIOs/itkGEAdwImageIOJSBindingWasm.js"},{"revision":"2c0f902b6d22b41c5f0d2825ef71ca95","url":"/rapid-reader/itk/ImageIOs/itkGEAdwImageIOJSBindingWasm.wasm"},{"revision":"ea01639a148493d398ea3916a6ab8a07","url":"/rapid-reader/itk/ImageIOs/itkGiplImageIOJSBinding.js"},{"revision":"8290084bed89727971457c60007cb08f","url":"/rapid-reader/itk/ImageIOs/itkGiplImageIOJSBindingWasm.js"},{"revision":"ba28c2e3beca19ad1c95ae2f13d2c633","url":"/rapid-reader/itk/ImageIOs/itkGiplImageIOJSBindingWasm.wasm"},{"revision":"68bf68b587f7c21912380686643a8840","url":"/rapid-reader/itk/ImageIOs/itkHDF5ImageIOJSBindingWasm.js"},{"revision":"bebf6fe92c0efd494e88b0ddbac4b770","url":"/rapid-reader/itk/ImageIOs/itkJPEGImageIOJSBinding.js"},{"revision":"a66b65e2fcb462349ee400ce4587d9e0","url":"/rapid-reader/itk/ImageIOs/itkJPEGImageIOJSBindingWasm.js"},{"revision":"2f88b62156a70f2e832342c4141c9b7d","url":"/rapid-reader/itk/ImageIOs/itkJPEGImageIOJSBindingWasm.wasm"},{"revision":"4319de03c695d34940b0ed3e68874c42","url":"/rapid-reader/itk/ImageIOs/itkJSONImageIOJSBinding.js"},{"revision":"338b3997213233c487988da184366abe","url":"/rapid-reader/itk/ImageIOs/itkJSONImageIOJSBindingWasm.js"},{"revision":"78ee18e409fa24b2a34568b0e4aed64c","url":"/rapid-reader/itk/ImageIOs/itkJSONImageIOJSBindingWasm.wasm"},{"revision":"8e8a530f2bb303742c7bf372c642a612","url":"/rapid-reader/itk/ImageIOs/itkLSMImageIOJSBinding.js"},{"revision":"620baca85bd648edff5ac071491843b0","url":"/rapid-reader/itk/ImageIOs/itkLSMImageIOJSBindingWasm.js"},{"revision":"3100035d4984e294591cbfa80420a563","url":"/rapid-reader/itk/ImageIOs/itkLSMImageIOJSBindingWasm.wasm"},{"revision":"da55fe91696febd9736369f1fc5fea28","url":"/rapid-reader/itk/ImageIOs/itkMGHImageIOJSBinding.js"},{"revision":"066a3293f055a873fb8166bf2406d1d0","url":"/rapid-reader/itk/ImageIOs/itkMGHImageIOJSBindingWasm.js"},{"revision":"a6a4c2a430c27502a0620c54d401bdc8","url":"/rapid-reader/itk/ImageIOs/itkMGHImageIOJSBindingWasm.wasm"},{"revision":"5ed23c65880c51a8b0c448685072e60a","url":"/rapid-reader/itk/ImageIOs/itkMINCImageIOJSBindingWasm.js"},{"revision":"2c4a8f1546e29819c5ea55f53cffbdc9","url":"/rapid-reader/itk/ImageIOs/itkMRCImageIOJSBinding.js"},{"revision":"db1fb5fdb51b0f46e4bce1c041090bbb","url":"/rapid-reader/itk/ImageIOs/itkMRCImageIOJSBindingWasm.js"},{"revision":"fb3f8dd34d3e89093d9d73c9dc5a0499","url":"/rapid-reader/itk/ImageIOs/itkMRCImageIOJSBindingWasm.wasm"},{"revision":"b553c0fc6f8c193bb2d7c38735e03edd","url":"/rapid-reader/itk/ImageIOs/itkMetaImageIOJSBinding.js"},{"revision":"bb115a30c2bae44b7e6d85efd4337c44","url":"/rapid-reader/itk/ImageIOs/itkMetaImageIOJSBindingWasm.js"},{"revision":"1b463332516f7ea8f314c2cedb147b03","url":"/rapid-reader/itk/ImageIOs/itkMetaImageIOJSBindingWasm.wasm"},{"revision":"41196bf3c1261b6796f2c856754879be","url":"/rapid-reader/itk/ImageIOs/itkNiftiImageIOJSBinding.js"},{"revision":"78938be8838b0416daa2d8c4fb9cbcf3","url":"/rapid-reader/itk/ImageIOs/itkNiftiImageIOJSBindingWasm.js"},{"revision":"284e856e0464e82c2aa805f5670f853b","url":"/rapid-reader/itk/ImageIOs/itkNiftiImageIOJSBindingWasm.wasm"},{"revision":"cde2a0c5b66ba235ede0f5726bcd6c4a","url":"/rapid-reader/itk/ImageIOs/itkNrrdImageIOJSBinding.js"},{"revision":"af5f6d4514576627afe2a2f98db2965b","url":"/rapid-reader/itk/ImageIOs/itkNrrdImageIOJSBindingWasm.js"},{"revision":"6270cac77f518baae515bb4162c06b5b","url":"/rapid-reader/itk/ImageIOs/itkNrrdImageIOJSBindingWasm.wasm"},{"revision":"d9701c44abb263be72ccee3b4ca13fe4","url":"/rapid-reader/itk/ImageIOs/itkPNGImageIOJSBinding.js"},{"revision":"ffa27960aa32dec86f682f78263e9a30","url":"/rapid-reader/itk/ImageIOs/itkPNGImageIOJSBindingWasm.js"},{"revision":"7cee8a642f98e0d7b7e573c72ae65b2f","url":"/rapid-reader/itk/ImageIOs/itkPNGImageIOJSBindingWasm.wasm"},{"revision":"7453e16179ac77917822c82610d25632","url":"/rapid-reader/itk/ImageIOs/itkScancoImageIOJSBinding.js"},{"revision":"4f70a0909d1d5c8e2d2535e820c87df9","url":"/rapid-reader/itk/ImageIOs/itkScancoImageIOJSBindingWasm.js"},{"revision":"40c5519e75d771f1d1de0fd8d2f3242c","url":"/rapid-reader/itk/ImageIOs/itkScancoImageIOJSBindingWasm.wasm"},{"revision":"ea78484d2660a4d182ae1dbcf64b3a02","url":"/rapid-reader/itk/ImageIOs/itkTIFFImageIOJSBinding.js"},{"revision":"a70fc769be360573b9fffe4fee1e362c","url":"/rapid-reader/itk/ImageIOs/itkTIFFImageIOJSBindingWasm.js"},{"revision":"93323bf6445720e8fe2dd8c8e4401144","url":"/rapid-reader/itk/ImageIOs/itkTIFFImageIOJSBindingWasm.wasm"},{"revision":"afc75e69fd08d4359b301306ca958370","url":"/rapid-reader/itk/ImageIOs/itkVTKImageIOJSBinding.js"},{"revision":"31537db81b8c360c20d6ab65032871f0","url":"/rapid-reader/itk/ImageIOs/itkVTKImageIOJSBindingWasm.js"},{"revision":"70018441f13fb4ef9eaf58c7c9b68751","url":"/rapid-reader/itk/ImageIOs/itkVTKImageIOJSBindingWasm.wasm"},{"revision":"74337e164a3653cb11013e1dec5ac4c2","url":"/rapid-reader/itk/WebWorkers/ImageIO.worker.js"},{"revision":"ccceb29c6cb00df18219d6c968fad639","url":"/rapid-reader/itk/WebWorkers/MeshIO.worker.js"},{"revision":"8e61dae81cdf1e0692a3258971942cad","url":"/rapid-reader/itk/WebWorkers/Pipeline.worker.js"},{"revision":"870f848acf5470b2cf369f6604fac737","url":"/rapid-reader/manifest.json"},{"revision":"754d698a7b334af57c00f29723fd9751","url":"/rapid-reader/oidc-client.min.js"},{"revision":"d05a380d50b74e629738ae6f62fb7e78","url":"/rapid-reader/polyfill.min.js"},{"revision":"f528b6861c82ee4415fce0821fd695c1","url":"/rapid-reader/silent-refresh.html"},{"revision":"a5795282040aa9a0e9f527a16ec7d14c","url":"/rapid-reader/vendors~ConnectedStandaloneRouting~ConnectedXNATStandaloneRouting~IHEInvokeImageDisplay~StudyListRou~f8ce0673.bundle.fd5ecf2e0d61d74af917.js"},{"revision":"a1b89d0ffde2f8f084414c79e4973494","url":"/rapid-reader/vendors~ViewerLocalFileData.bundle.f83a8d1c5ffc6b9599c6.js"},{"revision":"7bfdd4458c6b1f707c66de74cb521c83","url":"/rapid-reader/vendors~dicom-microscopy-viewer.bundle.1c1063339c3db92fedd5.js"}]); // TODO: Cache API
// https://developers.google.com/web/fundamentals/instant-and-offline/web-storage/cache-api
// Store DICOMs?
// Clear Service Worker cache?
// navigator.storage.estimate().then(est => console.log(est)); (2GB?)

/***/ })
/******/ ]);