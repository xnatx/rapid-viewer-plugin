package org.nrg.xnatx.plugins.rapidReader.utils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.nrg.xnatx.plugins.rapidReader.dto.WorkItemDto;
import org.nrg.xnatx.plugins.rapidReader.dto.WorkListDto;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList;
import org.nrg.xnatx.plugins.rapidReader.entities.WorkList.WorkListStatus;
import org.nrg.xnatx.plugins.rapidReader.rest.RetrieveUtil;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

public class FileUtilsTest {


	@Test
	public void testFileUtils() throws IOException {
		// Given
		MultipartFile multipartFile = new MockMultipartFile("sourceFile.tmp", "Hello World".getBytes());

		File file = FileUtils.convert(multipartFile);

		// Then
		assertNotNull(file);
	}

}
