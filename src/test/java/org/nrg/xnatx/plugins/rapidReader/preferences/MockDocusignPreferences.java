package org.nrg.xnatx.plugins.rapidReader.preferences;

import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.AbstractDocusignPreferences;

public class MockDocusignPreferences implements AbstractDocusignPreferences {
    private String privateKey;

    private String integrationKey;

    private String apiUsername;

    private String authServiceUri;

    private int expiresAfterSec;

    private String scope;

    public MockDocusignPreferences(String privateKey, String integrationKey, String apiUsername, String authServiceUri, int expiresAfterSec, String scope) {
        this.privateKey = privateKey;
        this.integrationKey = integrationKey;
        this.apiUsername = apiUsername;
        this.authServiceUri = authServiceUri;
        this.expiresAfterSec = expiresAfterSec;
        this.scope = scope;
    }

    @Override
    public String getPrivateKey() {
        return privateKey;
    }

    @Override
    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Override
    public String getIntegrationKey() {
        return integrationKey;
    }

    @Override
    public void setIntegrationKey(String integrationKey) {
        this.integrationKey = integrationKey;
    }

    @Override
    public String getApiUsername() {
        return apiUsername;
    }

    @Override
    public void setApiUsername(String apiUsername) {
        this.apiUsername = apiUsername;
    }

    @Override
    public String getAuthServiceUri() {
        return authServiceUri;
    }

    @Override
    public void setAuthServiceUri(String authServiceUri) {
        this.authServiceUri = authServiceUri;
    }

    @Override
    public Integer getExpiresAfterSec() {
        return expiresAfterSec;
    }

    @Override
    public void setExpiresAfterSec(Integer expiresAfterSec) {
        this.expiresAfterSec = expiresAfterSec;
    }

    @Override
    public String getScope() {
        return scope;
    }

    @Override
    public void setScope(String scope) {
        this.scope = scope;
    }
}
