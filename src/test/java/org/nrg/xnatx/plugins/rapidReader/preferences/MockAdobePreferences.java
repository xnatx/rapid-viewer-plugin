package org.nrg.xnatx.plugins.rapidReader.preferences;

import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.AbstractAdobePreferences;
import org.nrg.xnatx.plugins.rapidReader.docusign.preferences.AbstractDocusignPreferences;

public class MockAdobePreferences implements AbstractAdobePreferences {
    private String code;
    private String apiAccessPoint;
    private String webAccessPoint;
    private String clientId;
    private String clientSecret;
    private String refreshToken;

    public MockAdobePreferences(String code, String apiAccessPoint, String webAccessPoint, String clientId, String clientSecret, String refreshToken) {
        this.code = code;
        this.apiAccessPoint = apiAccessPoint;
        this.webAccessPoint = webAccessPoint;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.refreshToken = refreshToken;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getApiAccessPoint() {
        return apiAccessPoint;
    }

    @Override
    public void setApiAccessPoint(String apiAccessPoint) {
        this.apiAccessPoint = apiAccessPoint;
    }

    @Override
    public String getWebAccessPoint() {
        return webAccessPoint;
    }

    @Override
    public void setWebAccessPoint(String webAccessPoint) {
        this.webAccessPoint = webAccessPoint;
    }

    @Override
    public String getClientId() {
        return clientId;
    }

    @Override
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String getClientSecret() {
        return clientSecret;
    }

    @Override
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Override
    public String getRefreshToken() {
        return refreshToken;
    }

    @Override
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }
}
